
SURFSLICE=shared/lib/surfslice.scm
SPRITESHEET=shared/lib/spritesheet.scm

default:
	@echo "Available examples:"
	@echo "  make eggsweeper"
	@echo "  make mandelbrot"
	@echo "  make platformer"
	@echo "  make shmup"

clean:
	rm -f eggsweeper/eggsweeper mandelbrot/mandelbrot
	make -C platformer clean
	make -C shmup clean

.PHONY: eggsweeper mandelbrot platformer shmup


eggsweeper: eggsweeper/eggsweeper
	cd eggsweeper && ./eggsweeper

eggsweeper/eggsweeper: eggsweeper/eggsweeper.scm $(SURFSLICE) $(SPRITESHEET)
	cd eggsweeper && csc -O3 eggsweeper.scm


mandelbrot: mandelbrot/mandelbrot
	cd mandelbrot && ./mandelbrot

mandelbrot/mandelbrot: mandelbrot/mandelbrot.scm
	csc -O3 mandelbrot/mandelbrot.scm


platformer:
	make -C platformer

shmup:
	make -C shmup
