;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A body represents the physics properties of an entity, such as its
;;; position, velocity, acceleration, mass, and speed limit.
;;;
;;; p, v, and a are sdl2:points in worldspace.

(define-record-type body
  (%make-body p v a m sl)
  body?
  (p  body-p  (setter body-p))          ; position
  (v  body-v  (setter body-v))          ; velocity
  (a  body-a  (setter body-a))          ; acceleration
  (m  body-m  (setter body-m))          ; mass
  (sl body-sl (setter body-sl))         ; speed limit
  )

(define-type body (struct body))

(: %make-body
   (point point point number number
    -> body))


(define (make-body #!key (p (P)) (v (P)) (a (P))
                   (m 1.0) (sl (s->w 200)))
  (assert (not (negative? sl)))
  (%make-body p v a m sl))


(define-record-printer (body b out)
  (fprintf out
    "#<body p: ~S v: ~S>"
    (sdl2:point->list (body-p b))
    (sdl2:point->list (body-v b))))


;;; Update the body's velocity and position. dt is delta time, the
;;; number of seconds that have passed since the last update.
(define (update-body! body dt)
  ;; Update velocity by applying acceleration.
  (sdl2:point-add! (body-v body) (sdl2:point-scale (body-a body) dt))

  ;; Ensure new velocity does not exceed body's speed limit.
  (body-limit-speed! body)

  ;; Update position by applying velocity.
  (sdl2:point-add! (body-p body) (sdl2:point-scale (body-v body) dt)))


;; Adjust body's velocity so it does not exceed its speed limit.
(define (body-limit-speed! body)
  (let* ((vx (sdl2:point-x (body-v body)))
         (vy (sdl2:point-y (body-v body)))
         (sp^2 (+ (sq vx) (sq vy)))
         (sl^2 (sq (body-sl body))))
    ;; If speed squared is greater than speed limit squared, then
    ;; scale velocity so speed will equal the speed limit.
    (when (>= sp^2 sl^2)
      (sdl2:point-scale! (body-v body) (sqrt (/ sl^2 sp^2))))))
