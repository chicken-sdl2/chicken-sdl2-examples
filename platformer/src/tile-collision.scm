;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines procedures for detecting and resolving
;;; collisions between an entity and a tile.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BROAD PHASE TILE COLLISION DETECTION
;;;
;;; In the broad phase of tile collision detection, we look for tiles
;;; that might POSSIBLY collide with an entity, using relatively fast
;;; math. This is an initial pass to reduce the number of tiles that
;;; must be considered during the narrow phase.


;;; Return the range of tile columns and rows that might possibly
;;; collide with the entity.
;;; Returns 4 values: min-col max-col min-row max-row.
(: nearby-tile-ranges
   (entity -> fixnum fixnum fixnum fixnum))
(define (nearby-tile-ranges entity)
  (let ((bbox (entity-bbox entity)))
    (values (floor (w->g (sdl2:rect-x bbox)))
            (floor (w->g (rect-right  bbox)))
            (floor (w->g (sdl2:rect-y bbox)))
            (floor (w->g (rect-bottom bbox))))))


;;; Returns a list of solid tiles in the level that are near the
;;; entity. Nonsolid tiles are omitted because they do not need to be
;;; checked for collision.
(: nearby-solid-tiles
   (entity level -> (list-of tile)))
(define (nearby-solid-tiles entity level)
  (receive (min-col max-col min-row max-row)
      (nearby-tile-ranges entity)
    ;; Clamp to the dimensions of the level so that we don't try to
    ;; get any out-of-bounds tiles.
    (let ((min-col (max min-col 0))
          (max-col (min max-col (sub1 (level-cols level))))
          (min-row (max min-row 0))
          (max-row (min max-row (sub1 (level-rows level)))))
      ;; TODO: Maybe rewrite this in a more functional style.
      (let ((results (make-parameter '())))
        ;; Iterate through every row and column.
        (do ((row min-row (add1 row)))
            ((> row max-row))
          (do ((col min-col (add1 col)))
              ((> col max-col))
            (let ((tile (level-ref level col row)))
              ;; Include the tile in the results if it is solid.
              (when (not (tile-prop tile 'nonsolid))
                (results (cons tile (results)))))))
        (results)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NARROW PHASE TILE COLLISION DETECTION
;;;
;;; In the rawwon phase of tile collision detection, we precisely test
;;; each tile that was detected during the broad phase, to see whether
;;; it actually collides with the entity or not.


;;; Check entity/tile collisions. Returns an alist of (symbol . rect)
;;; pairs for every hitbox of the entity that collides with the tile.
;;; Returns an empty list if there is no collision.
(: check-e/t-colls
   (entity tile -> (list-of (pair symbol rect))))
(define (check-e/t-colls entity tile)
  (let ((tile-hb      (tile-hitbox tile))
        (hitbox-pairs (entity-hitboxes entity)))
    (filter (lambda (hitbox-pair)
              (sdl2:has-intersection? tile-hb (cdr hitbox-pair)))
            hitbox-pairs)))
