;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define +player-laser-speed+ (s->w 500))


(define (make-laser #!key owner color
                    (pos (P 0 0))
                    (vel (P 0 0))
                    (collider void))
  (make-entity
   id:      (gensym 'laser)
   type:    'laser
   texture: (laser-texture color)
   pos:     pos
   vel:     vel
   scale:   0.5
   updater: laser-updater
   collider: collider
   props:   (list (cons 'color color)
                  (cons 'owner owner))))

(define (spawn-laser! scene owner)
  (let ((laser
         (make-laser
          owner: owner
          color: (entity-prop owner 'color)
          pos:   (entity-pos owner)
          vel:   (P 0 (- +player-laser-speed+))
          collider: (if (eq? 'player (entity-type owner))
                        player-laser-collider
                        void))))
    (apply scene-add-entity! scene laser)
    laser))


(define (laser-updater laser dt)
  (update-entity-pos! laser dt)
  (unless (entity-in-scene-bounds? laser (*scene*))
    (destroy-entity! laser)))


(define (player-laser-collider laser other)
  (case (entity-type other)
    ((enemy)
     (destroy-entity! other)
     (destroy-entity! laser))))
