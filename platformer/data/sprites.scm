;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines all the available sprites in the spritesheet.


(image-path "../shared/assets/kenney/platformer_pixel/spritesheet.png")
(color-key 86 128 163)
(settings cell-w:   21  cell-h:   21
          gap-x:    2   gap-y:    2
          offset-x: 2   offset-y: 2)

(sprites
 ;; (Alignment helper) ................ col: .... row:

 ;; Players
 (player-green-front                    col: 19   row: 0)
 (player-green-stand                    col: 20   row: 0)
 (player-green-jump                     col: 21   row: 0)
 (player-green-duck                     col: 22   row: 0)
 (player-green-hurt                     col: 23   row: 0)
 (player-green-climb1                   col: 24   row: 0)
 (player-green-climb2                   col: 25   row: 0)
 (player-green-swim1                    col: 26   row: 0)
 (player-green-swim2                    col: 27   row: 0)
 (player-green-walk1                    col: 28   row: 0)
 (player-green-walk2                    col: 29   row: 0)

 (player-blue-front                     col: 19   row: 1)
 (player-blue-stand                     col: 20   row: 1)
 (player-blue-jump                      col: 21   row: 1)
 (player-blue-duck                      col: 22   row: 1)
 (player-blue-hurt                      col: 23   row: 1)
 (player-blue-climb1                    col: 24   row: 1)
 (player-blue-climb2                    col: 25   row: 1)
 (player-blue-swim1                     col: 26   row: 1)
 (player-blue-swim2                     col: 27   row: 1)
 (player-blue-walk1                     col: 28   row: 1)
 (player-blue-walk2                     col: 29   row: 1)

 (player-pink-front                     col: 19   row: 2)
 (player-pink-stand                     col: 20   row: 2)
 (player-pink-jump                      col: 21   row: 2)
 (player-pink-duck                      col: 22   row: 2)
 (player-pink-hurt                      col: 23   row: 2)
 (player-pink-climb1                    col: 24   row: 2)
 (player-pink-climb2                    col: 25   row: 2)
 (player-pink-swim1                     col: 26   row: 2)
 (player-pink-swim2                     col: 27   row: 2)
 (player-pink-walk1                     col: 28   row: 2)
 (player-pink-walk2                     col: 29   row: 2)

 (player-yellow-front                   col: 19   row: 3)
 (player-yellow-stand                   col: 20   row: 3)
 (player-yellow-jump                    col: 21   row: 3)
 (player-yellow-duck                    col: 22   row: 3)
 (player-yellow-hurt                    col: 23   row: 3)
 (player-yellow-climb1                  col: 24   row: 3)
 (player-yellow-climb2                  col: 25   row: 3)
 (player-yellow-swim1                   col: 26   row: 3)
 (player-yellow-swim2                   col: 27   row: 3)
 (player-yellow-walk1                   col: 28   row: 3)
 (player-yellow-walk2                   col: 29   row: 3)

 (player-beige-front                    col: 19   row: 4)
 (player-beige-stand                    col: 20   row: 4)
 (player-beige-jump                     col: 21   row: 4)
 (player-beige-duck                     col: 22   row: 4)
 (player-beige-hurt                     col: 23   row: 4)
 (player-beige-climb1                   col: 24   row: 4)
 (player-beige-climb2                   col: 25   row: 4)
 (player-beige-swim1                    col: 26   row: 4)
 (player-beige-swim2                    col: 27   row: 4)
 (player-beige-walk1                    col: 28   row: 4)
 (player-beige-walk2                    col: 29   row: 4)

 ;; Grass tiles
 (tile-grass-ledge-left                 col: 0    row: 4)
 (tile-grass                            col: 1    row: 4)
 (tile-grass-left                       col: 2    row: 4)
 (tile-grass-mid                        col: 3    row: 4)
 (tile-grass-right                      col: 4    row: 4)
 (tile-grass-cliff-left                 col: 5    row: 4)
 (tile-grass-hill-left                  col: 6    row: 4)
 (tile-grass-hill-right                 col: 7    row: 4)
 (tile-grass-half                       col: 8    row: 4)
 (tile-grass-half-left                  col: 9    row: 4)
 (tile-grass-ledge-right                col: 0    row: 5)
 (tile-grass-cliff-alt-left             col: 1    row: 5)
 (tile-grass-center                     col: 2    row: 5)
 (tile-grass-cliff-alt-right            col: 3    row: 5)
 (tile-grass-center-round               col: 4    row: 5)
 (tile-grass-cliff-right                col: 5    row: 5)
 (tile-grass-hill-left2                 col: 6    row: 5)
 (tile-grass-hill-right2                col: 7    row: 5)
 (tile-grass-half-mid                   col: 8    row: 5)
 (tile-grass-half-right                 col: 9    row: 5)

 ;; Water and lava tiles
 (tile-water-top-round                  col: 10   row: 0)
 (tile-water-top                        col: 11   row: 0)
 (tile-water                            col: 10   row: 1)
 (tile-lava-top-round                   col: 12   row: 0)
 (tile-lava-top                         col: 13   row: 0)
 (tile-lava                             col: 12   row: 1)

 ;; Bridge and ladder tiles
 (tile-bridge                           col: 11   row: 1)
 (tile-bridge-logs                      col: 13   row: 1)
 (tile-ladder-mid                       col: 12   row: 3)
 (tile-ladder-top                       col: 13   row: 3)

 ;; Items
 (item-coin-bronze                      col: 16   row: 2)
 (item-coin-silver                      col: 17   row: 2)
 (item-coin-gold                        col: 18   row: 2)
 (item-coin-star                        col: 16   row: 3)
 (item-bomb                             col: 17   row: 8)
 (item-bomb-flash                       col: 18   row: 8)
 (item-springboard-down                 col: 13   row: 9)
 (item-springboard-up                   col: 14   row: 9)
 (item-gem-yellow                       col: 15   row: 9)
 (item-gem-green                        col: 16   row: 9)
 (item-gem-red                          col: 17   row: 9)
 (item-gem-blue                         col: 18   row: 9)

 ;; Ice tiles
 (tile-ice                              col: 10   row: 16)
 (tile-ice2                             col: 11   row: 16)
 (tile-ice-half                         col: 12   row: 16)
 (tile-ice-half2                        col: 13   row: 16)

 ) ;; end sprites
