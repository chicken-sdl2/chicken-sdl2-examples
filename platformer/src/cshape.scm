;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A cshape ("collision shape") represents a shape used for collision
;;; detection. Every collidable entity has a cshape.
;;;
;;; A cshape has a w (width) and h (height), which are worldspace
;;; dimensions used to generate a bounding box for broad phase
;;; collision detection.
;;;
;;; A cshape has an alist of "hitboxes", which are worldspace rects
;;; used during narrow phase collision detection. The rects are
;;; defined relative to the entity's center. The car of each pair in
;;; the alist is a symbol label used to distinguish the hitboxes for
;;; collision resolution and gameplay logic.


(define-record-type cshape
  (%make-cshape hitboxes)
  cshape?
  (hitboxes cshape-hitboxes (setter cshape-hitboxes))
  (bbox     cshape-bbox     (setter cshape-bbox))) ; bbox cache

(define-type cshape (struct cshape))


(: make-cshape
   ((list-of (pair symbol (struct sdl2:rect)))
    -> cshape))
(define (make-cshape hitboxes)
  (let ((c (%make-cshape hitboxes)))
    (set! (cshape-bbox c) (cshape-calculate-bbox! c))
    c))


(define-record-printer (cshape c out)
  (fprintf out
    "#<cshape ~Sx~S (~S hitboxes)>"
    (cshape-w c) (cshape-h c)
    (length (cshape-hitboxes c))))


(define (cshape-w cshape)
  (sdl2:rect-w (cshape-bbox cshape)))

(define (cshape-h cshape)
  (sdl2:rect-h (cshape-bbox cshape)))


;;; Calculate the cshape's bounding box, in worldspace coordinates
;;; relative to the entity's center. This is the union of all the
;;; cshape's hitboxes.
(define (cshape-calculate-bbox! cshape)
  (if (= 1 (length (cshape-hitboxes cshape)))
      (cdar (cshape-hitboxes cshape))
      (fold sdl2:union-rect
            (sdl2:make-rect)
            (map cdr (cshape-hitboxes cshape)))))
