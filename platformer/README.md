
# Treasure Jumpers (platformer)

![Screenshot of Treasure Jumpers](screenshot.png)

Treasure Jumpers is a simple, local-multiplayer action platformer game created by John Croisant for the
[January 2016 Lisp Game Jam](http://itch.io/jam/january-2016-lisp-game-jam).
It was made with [CHICKEN Scheme](http://call-cc.org/), [SDL2](http://www.libsdl.org/),
and [chicken-sdl2](https://gitlab.com/chicken-sdl2/chicken-sdl2).
You can [read about its development on John's blog](http://blog.jacius.info/tag/treasure-jumpers/).

The [source code](https://gitlab.com/chicken-sdl2/chicken-sdl2-examples/tree/master/platformer)
is available under the
[CC0 1.0 Universal Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/).
Image assets are from [Kenney Game Assets](http://kenney.itch.io/kenney-donation).
The assets are also available under the CC0 1.0 Universal Public Domain Dedication.

Treasure Jumpers is intended as an example for using chicken-sdl2.
The source code is therefore heavily commented with explanations.
You are welcome to borrow freely from the code when making your own games.


## Requirements

Treasure Jumpers requires the following software:

- [CHICKEN Scheme](http://call-cc.org/) 4.8.0 or higher
- [SDL](https://libsdl.org/) 2.0 or higher
- [SDL_image](http://www.libsdl.org/projects/SDL_image/docs/index.html) 2.0 or higher
- The [`sdl2`](http://wiki.call-cc.org/eggref/4/sdl2), [`sdl2-image`](http://wiki.call-cc.org/eggref/4/sdl2-image), `args`, `miscmacros`, and `vector-lib` eggs for CHICKEN Scheme


## Running the game

1. Install CHICKEN Scheme, SDL, and SDL_image as appropriate for your operating system.

2. Install the required eggs:
   `chicken-install sdl2 sdl2-image args miscmacros vector-lib`

3. Compile Treasure Jumpers:
   `csc -O3 platformer.scm`

4. Play Treasure Jumpers:
   `./platformer`


## Default controls

Player 1 (pink):

    W = Jump
    A = Move left
    D = Move right

Player 2 (green):

    Up Arrow = Jump
    Left Arrow = Move left
    Right Arrow = Move right

Player 3 (yellow):

    I = Jump
    J = Move left
    L = Move right

You can change the key bindings by editing `controls.txt`.
Up to 5 players are supported.


## Custom levels

You can create your own levels using a text editor.
See the [level-template.txt](platformer/levels/level-template.txt)
file in the levels directory for more information.

I didn't have time to make a GUI level selector,
but you can select your level using a command line flag.
Compile the game as described above,
then run the game on the command line like so:

    ./platformer --level=path/to/your/level.txt

If you create a fun level and would like to have it included in the game,
please email john at croisant dot net.


## Known Issues

- The performance of the game gradually degrades over time.
  After several minutes the framerate becomes very choppy.

- There is no HUD or GUI to display player scores.

- There is no way to win. The game never ends.

- If you walk or slide off an ice tile without jumping,
  you will have slippery feet until the next time you jump.
