;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines constants and procedures for converting numbers,
;;; points, and rects between coordinate spaces. In particular, the
;;; following coordinate spaces are used:
;;;
;;; - Gridspace:   Rows and columns on the grid
;;; - Screenspace: Pixels on the screen
;;; - Worldspace:  High-precision units
;;;
;;; The only real difference between the coordinate spaces is their
;;; scale. So, converting from one space to another is simply a matter
;;; of multilpying or dividing by a scaling factor.
;;;
;;; NOTE: The window-scale setting is applied after the scene is
;;; rendered, so it does not affect converting to or from screenspace.



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WORLDSPACE
;;;
;;; Worldspace is a coordinate space used by entities in the game
;;; world. It is larger than screenspace. This allows us to have more
;;; precision while still using sdl2:rects and sdl2:points.

;;; Worldspace Scaling Factor.
(define +wsf+ 1000)

;;; Convert a number, point, or rect from screenspace to worldspace.
(define-inline (s->w   n) (* n +wsf+))
(define-inline (s->w/p p) (sdl2:point-scale p +wsf+))
(define-inline (s->w/r r) (sdl2:rect-scale  r +wsf+))

;;; Convert a number, point, or rect from worldspace to screenspace.
(define-inline (w->s   n) (/ n +wsf+))
(define-inline (w->s/p p) (sdl2:point-unscale p +wsf+))
(define-inline (w->s/r r) (sdl2:rect-unscale  r +wsf+))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GRIDSPACE
;;;
;;; Gridspace is the coordinate space for tiles on a grid. Coordinates
;;; in gridspace are measured in rows and columns.

;;; Grid size in screenspace coordinates (pixels).
(define +grid-size+   21)
(define +grid-size/2+ (fx/ +grid-size+ 2))

;;; Grid size in worldspace coordinates.
(define +grid-size-w+   (s->w +grid-size+))
(define +grid-size/2-w+ (fx/ (s->w +grid-size+) 2))


;;; Convert a number, point, or rect from screenspace to gridspace.
;;; The result will be a float if the screenspace coordinatess are not
;;; exactly on a tile corner. E.g. a column of 2 means exactly on the
;;; left edge of column 2 (and the right edge of column 1). Column 2.5
;;; would mean exactly in the middle of column 2.
(define-inline (s->g   n) (/ n +grid-size+))
(define-inline (s->g/p p) (sdl2:point-unscale p +grid-size+))
(define-inline (s->g/r r) (sdl2:rect-unscale  r +grid-size+))

;;; Convert a number, point, or rect from gridspace to worldspace.
(define-inline (g->s   n) (* n +grid-size+))
(define-inline (g->s/p p) (sdl2:point-scale p +grid-size+))
(define-inline (g->s/r r) (sd2l:rect-scale  r +grid-size+))


;;; Convert a number, point, or rect from worldspace to gridspace.
;;; The result will be a float if the worldspace coordinatess are not
;;; exactly on a tile corner.
(define-inline (w->g   n) (/ n +grid-size-w+))
(define-inline (w->g/p p) (sdl2:point-unscale p +grid-size-w+))
(define-inline (w->g/r r) (sdl2:rect-unscale  r +grid-size-w+))

;;; Convert a number, point, or rect from gridspace to worldspace.
(define-inline (g->w   n) (* n +grid-size-w+))
(define-inline (g->w/p p) (sdl2:point-scale p +grid-size-w+))
(define-inline (g->w/r r) (sdl2:rect-scale  r +grid-size-w+))
