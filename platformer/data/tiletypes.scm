;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/

;;; This file defines the tiletypes that can be used in a level.

;;; Empty space
(--- ()                            ((nonsolid . #t)))

;;; Grass
(G-- (tile-grass))
(GL- (tile-grass-left))
(GM- (tile-grass-mid))
(GR- (tile-grass-right))
(GC- (tile-grass-center))
(GCL (tile-grass-cliff-left))
(GCR (tile-grass-cliff-right))
(GHL (tile-grass-hill-left)        ((shape . slope-left)))
(GHR (tile-grass-hill-right)       ((shape . slope-right)))
(GHl (tile-grass-hill-left2))
(GHr (tile-grass-hill-right2))
(Gh- (tile-grass-half)             ((shape . top-half)))
(GhL (tile-grass-half-left)        ((shape . top-half)))
(GhM (tile-grass-half-mid)         ((shape . top-half)))
(GhR (tile-grass-half-right)       ((shape . top-half)))

;;; Water and lava
(W-- (tile-water)                  ((liquid . #t)))
(WT- (tile-water-top)              ((liquid . #t) (shape . bottom-half)))
(WTR (tile-water-top-round)        ((liquid . #t) (shape . bottom-half)))
(V-- (tile-lava)                   ((hazard . #t)))
(VT- (tile-lava-top)               ((hazard . #t) (shape . bottom-half)))
(VTR (tile-lava-top-round)         ((hazard . #t) (shape . bottom-half)))

;;; Bridge and ladder
(PB- (tile-bridge)                 ((shape . bottom-third)))
(PBl (tile-bridge-logs)            ((shape . bottom-third)))
(PLM (tile-ladder-mid)             ((climbable . #t)))
(PLT (tile-ladder-top)             ((climbable . #t)))

;;; Ice
(I-- (tile-ice)                    ((friction . 0)))
(I2- (tile-ice2)                   ((friction . 0)))
(Ih- (tile-ice-half)               ((friction . 0) (shape . top-half)))
(Ih2 (tile-ice-half2)              ((friction . 0) (shape . top-half)))
