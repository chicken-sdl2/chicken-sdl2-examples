;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines the handle-event! procedure, the global list
;;; procedures for handling various events, and other code related to
;;; event handling.


;;; Disable various irrelevant event types, to avoid wasted time and
;;; memory garbage from handling them.
(for-each (cut sdl2:event-state-set! <> #f)
          '(mouse-button-down
            mouse-button-up
            mouse-motion
            mouse-wheel
            finger-down
            finger-up
            finger-motion
            multi-gesture))

;;; We don't care about text input.
(sdl2:stop-text-input!)


;;; Parameter holding a list of global event handler procedures.
(define *event-handlers* (make-parameter '()))

;;; Return a list of the global event handler procedures.
(define (build-event-handlers)
  (cons* quit-event-handler
         escape-key-handler
         screenshot-event-handler
         player-event-handler
         (*controls-handlers*)))


;;; Handle a single event, by invoking handler procedures one after
;;; another until the event is consumed by one of the handlers. Once
;;; the event is consumed, no more handlers will be invoked and the
;;; return values of the last inoked handler will be returned. If no
;;; handlers consume the event, handle-event! returns #f.
;;;
;;; HANDLERS must be a list of procedures which accept a single
;;; sdl2:event as argument, and returns at least one value, a boolean
;;; indicating whether the event was consumed.
;;;
(: handle-event!
   ((struct sdl2:event)
    (list-of (procedure ((struct sdl2:event)) boolean))
    -> *))
(define (handle-event! ev handlers)
  ;; If there are no more handlers in the list, return #f.
  (if (null? handlers)
      #f
      ;; Otherwise, try invoking the first h
      (receive results ((first handlers) ev)
        ;; If the first return value of the handler is true, that
        ;; indicates the handler consumed the event, so return all the
        ;; return values from the handler.
        (if (and (not (null? results))
                 (first results))
            (apply values results)
            ;; Otherwise, recursively invoke handle-input-event
            ;; without the first handler.
            (handle-event! ev (cdr handlers))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GENERAL EVENT HANDLERS

;;; Aliases to make the event handling code more clear.
(define +event-consumed+     #t)
(define +event-not-consumed+ #f)


;; User requested app quit (e.g. clicked the close button, pressed
;; Escape, pressed Command-Q on Mac).
(define (quit-event-handler ev)
  (if (sdl2:quit-event? ev)
      ;; Call the (*exit-main-loop!*) continuation. Whoosh!
      ((*exit-main-loop!*) (void))
      +event-not-consumed+))


;; Escape pushes a quit event onto the queue.
(define (escape-key-handler ev)
  (if (and (eq? 'key-down (sdl2:event-type ev))
           (eq? 'escape (sdl2:keyboard-event-sym ev)))
      (begin
        (sdl2:push-event! (sdl2:make-event 'quit))
        +event-consumed+)
      +event-not-consumed+))


;; Ctrl-S saves a screenshot
(define (screenshot-event-handler ev)
  (if (and (eq? 'key-down (sdl2:event-type ev))
           (eq? 's (sdl2:keyboard-event-sym ev))
           (memq 'ctrl (sdl2:keyboard-event-mod ev)))
      (begin
        (save-screenshot! (*window-buffer*))
        +event-consumed+)
      +event-not-consumed+))

