;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; An entity is a moving object in a scene, such as a player or item.
;;; An entity has (for now) a sprite that defines its appearance, a
;;; body that defines its physical attributes, a list of forces that
;;; are affecting it, and a list of extra properties that can be used
;;; by the gameplay logic.


(define-record-type entity
  (%make-entity id type sprite body cshape forces props)
  entity?
  (id      entity-id      (setter entity-id))      ; symbol or #f
  (type    entity-type    (setter entity-type))    ; symbol
  (sprite  entity-sprite  (setter entity-sprite))  ; sprite symbol
  (body    entity-body    (setter entity-body))    ; body
  (cshape  entity-cshape  (setter entity-cshape))  ; cshape
  (forces  entity-forces  (setter entity-forces))  ; forces alist
  (hbcache entity-hbcache (setter entity-hbcache)) ; hitboxes cache
  (props   entity-props   (setter entity-props)))  ; properties alist

(define-type entity (struct entity))


(: make-entity
   ((or symbol boolean) symbol
    #!optional symbol body cshape
    (list-of (pair symbol (struct sdl2:point)))
    (list-of (pair symbol *))
    -> entity))
(define (make-entity id type #!optional
                     (sprite 'item-bomb)
                     (body (make-body))
                     (cshape (make-cshape '()))
                     (forces '())
                     (props '()))
  (let ((e (%make-entity id type sprite body cshape forces props)))
    (set! (entity-hbcache e) #f)
    e))


(compose-accessors entity-p (body-p entity-body))
(compose-accessors entity-v (body-v entity-body))
(compose-accessors entity-a (body-a entity-body))

(compose-accessors entity-px (sdl2:point-x entity-p))
(compose-accessors entity-py (sdl2:point-y entity-p))
(compose-accessors entity-vx (sdl2:point-x entity-v))
(compose-accessors entity-vy (sdl2:point-y entity-v))


;;; Return a rect representing the entity's bounding box, in world
;;; space coordinates.
(define (entity-bbox entity)
  (sdl2:rect-add-point
   (cshape-bbox (entity-cshape entity))
   (entity-p entity)))

;;; Return an list of (symbol . rect) pairs for all the entity's
;;; hitboxes, in worldspace coordinates.
(define (entity-hitboxes entity)
  (unless (entity-hbcache entity)
    (initialize-entity-hitboxes! entity))
  (entity-hbcache entity))


;;; Create the entity's initial hbcache.
(define (initialize-entity-hitboxes! entity)
  (set! (entity-hbcache entity)
    (map (lambda (hb)
           (cons (car hb)
                 (sdl2:rect-add-point (cdr hb) (entity-p entity))))
         (cshape-hitboxes (entity-cshape entity)))))

;;; Update the entity's cached hitboxes using its current position.
(define (update-entity-hitboxes! entity)
  (if (not (entity-hbcache entity))
      (initialize-entity-hitboxes! entity)
      (let ((ep   (entity-p entity))
            (ehbs (entity-hbcache entity))
            (chbs (cshape-hitboxes (entity-cshape entity))))
        (for-each (lambda (chb ehb)
                    (sdl2:rect-copy! (cdr chb) (cdr ehb))
                    (sdl2:rect-add-point! (cdr ehb) ep))
                  chbs
                  ehbs))))


(define-record-printer (entity e out)
  (fprintf out
    "#<entity ~S p: ~S>"
    (entity-id e)
    (sdl2:point->list (entity-p e))))


;;; Get a named property of the entity. Returns default if the
;;; property is not found in the entity.
(: entity-prop
   (entity symbol #!optional * -> *))
(define (entity-prop entity name #!optional default)
  (alist-ref name (entity-props entity) eqv? default))

;;; Set a property of the entity.
(: entity-prop-set!
   (entity symbol * -> void))
(define (entity-prop-set! entity name value)
  (set! (entity-props entity)
        (alist-update! name value (entity-props entity))))

(set! (setter entity-prop) entity-prop-set!)


;;; Return the acceleration (point) for the force with the given name,
;;; or #f if the entity has no force with that name.
(: entity-force (entity symbol -> (or point boolean)))
(define (entity-force entity force-name)
  (alist-ref force-name (entity-forces entity)))

;;; Add or update a force with the given name and acceleration. If the
;;; entity already has a force with this name, it is replaced with the
;;; new value.
(: entity-force-set! (entity symbol point -> void))
(define (entity-force-set! entity force-name force-accel)
  (set! (entity-forces entity)
    (alist-update! force-name force-accel (entity-forces entity)))
  (update-entity-accel! entity)
  (void))

(define (update-entity-accel! entity)
  (let ((a (body-a (entity-body entity))))
    (sdl2:point-set! a 0 0)
    (for-each (lambda (force-pair)
                (sdl2:point-add! a (cdr force-pair)))
              (entity-forces entity))))


;;; Update the entity. dt is delta time, the number of seconds that
;;; have passed since the last update.
(define (update-entity! entity dt)
  (unless (entity-prop entity 'static)
    (update-body! (entity-body entity) dt))
  (update-entity-hitboxes! entity))


(define (draw-entity! entity dst #!optional (off-x 0) (off-y 0))
  (let ((x (+ off-x (- +grid-size/2+) (w->s (entity-px entity))))
        (y (+ off-y (- +grid-size/2+) (w->s (entity-py entity)))))
    (blit-sprite! (entity-sprite entity) dst
                  (R (round x) (round y)
                     +grid-size+ +grid-size+))))


(define (entity-move-to-tile! entity tile)
  (set! (entity-px entity) (g->w (+ 0.5 (tile-col tile))))
  (set! (entity-py entity) (g->w (+ 0.5 (tile-row tile)))))
