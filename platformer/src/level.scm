;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A level is a collection of tiles arranged in a grid. A level is
;;; not responsible for managing entities like players or items, only
;;; the relatively passive things like dirt, water, and spawn points
;;; for players or items.
;;;
;;; A level has an id symbol for human reference. This is usually
;;; derived from the level's filename at load time.
;;;
;;; A level has dimensions: the number of cols (columns) and rows.
;;; This affects the number of tiles in the level.
;;;
;;; A level has a linear vector of tiles, ordered from left to right,
;;; top to bottom.
;;;
;;; A level has an alist of extra properties, which can be used for
;;; metadata and for game logic. For example there may be a property
;;; giving the name of the level author. And there may be a property
;;; indicating how many players the level is designed for.


(define-record-type level
  (%make-level name cols rows tiles props)
  level?
  (name  level-name)                        ; level name string
  (cols  level-cols)                        ; # of columns (width)
  (rows  level-rows)                        ; # of rows (height)
  (tiles level-tiles)                       ; flat vector of tiles
  (props level-props (setter level-props))) ; properties alist

(define-type level (struct level))


(: make-level
   (string fixnum fixnum #!optional (list-of (pair symbol *))
    -> level))
(define (make-level name cols rows #!optional (props '()))
  (let ((empty-type (get-tiletype '---)))
    (%make-level
     name cols rows
     (vector-unfold (lambda (i)
                      (let ((col (modulo i cols))
                            (row (floor (/ i cols))))
                        (make-tile #f empty-type col row)))
                    (* cols rows))
     props)))


(define-record-printer (level m out)
  (fprintf out "#<level ~S ~Sx~S>"
           (level-name m) (level-cols m) (level-rows m)))


;;; Returns true if the given column and row are in bounds
(define-inline (level-in-bounds? level col row)
  (and (< -1 row (level-rows level))
       (< -1 col (level-cols level))))


;;; Convert col and row into a linear index for the tiles vector.
(define-inline (%level-index level col row)
  (exact (+ col (* row (level-cols level)))))


;;; Signals an error if the given col or row is out of bounds.
(define-inline (%assert-level-bounds! level col row)
  (unless (level-in-bounds? level col row)
    (error (sprintf "Location ~S,~S is out of bounds for level" col row)
           level)))

;;; Return the tile at the given column and row. Signals error if col
;;; or row is out of bounds.
(define (level-ref level col row)
  (%assert-level-bounds! level col row)
  (vector-ref (level-tiles level) (%level-index level col row)))

;;; Set the tile at the given column and row. Signals error if col or
;;; row is out of bounds.
(define (level-set! level col row tile)
  (%assert-level-bounds! level col row)
  (assert (tile? tile))
  (set! (vector-ref (level-tiles level) (%level-index level col row))
        tile))

(set! (setter level-ref) level-set!)


;;; Get a property from the level. Returns default if the property is
;;; not found.
(: level-prop
   (level symbol #!optional * -> *))
(define (level-prop level name #!optional default)
  (alist-ref name (level-props level) eqv? default))

;;; Set a property of the level.
(: level-prop-set!
   (level symbol * -> void))
(define (level-prop-set! level name value)
  (set! (level-props level)
        (alist-update! name value (level-props level))))

(set! (setter level-prop) level-prop-set!)


(define +base-gravity+ 450)

(define (level-gravity level)
  (s->w/p (P 0 (round (* +base-gravity+ (level-prop level 'gravity 1))))))


;;; Draw all the tiles of the level to the destination surface. off-x
;;; and off-y are screenspace offsets for drawing.
(define (draw-level! level dst #!optional (off-x 0) (off-y 0))
  ;; Fill background color
  (sdl2:fill-rect! (*level-buffer*) #f
                   (level-bg-color level))

  ;; Draw all tiles
  (vector-for-each (lambda (i tile)
                     (draw-tile! tile dst off-x off-y))
                   (level-tiles level)))


(define (level-bg-color level)
  (let ((bg (level-prop level 'bg-color '(0 0 0))))
    (if (and (list? bg) (= 3 (length bg)) (every integer? bg))
        (apply sdl2:make-color bg)
        (begin
          (fprintf (current-error-port)
            "Warning: level has invalid bg-color prop: ~S~N" bg)
          (C 0 0 0)))))


(define (level-height level)
  (s->w (* +grid-size+ (level-rows level))))


;;; Return a list of all tiles which have a label that is found in the
;;; given list of labels, in order from left to right, top to bottom.
(define (level-tiles-with-matching-label level labels)
  (reverse!
   (vector-fold
    (lambda (index accum tile)
      (if (member (tile-label tile) labels)
          (cons tile accum)
          accum))
    '()
    (level-tiles level))))


;;; Returns a list of all tiles that are labelled as a primary spawn
;;; point for the given player. Returns empty list if none are found.
(define (level-player-primary-spawns level player)
  (level-tiles-with-matching-label
   level (list (player-primary-spawn-label player))))

;;; Returns a list of all tiles that are labelled as a secondary spawn
;;; point for any player. Returns empty list if none are found.
(define (level-player-secondary-spawns level)
  (level-tiles-with-matching-label
   level (list +player-secondary-spawn-label+)))


;;; Returns a list of all tiles that are labelled as a treasure spawn
;;; point. Returns empty list if none are found.
(define (level-treasure-spawns level)
  (level-tiles-with-matching-label
   level +treasure-spawn-labels+))


;;; Returns a tile in the middle of the level. This is used as a
;;; fallback player spawn point in case the level has none.
(define (level-middle-tile level)
  (level-ref level
             (fx/ (level-cols level) 2)
             (fx/ (level-rows level) 2)))

;;; Returns a random tile in the level. This is used as a fallback
;;; treasure spawn point in case the level has none.
(define (level-random-tile level)
  (vector-pick-random (level-tiles level)))
