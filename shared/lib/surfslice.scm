;;; This is an example library for use with chicken-sdl2.
;;; It is distributed with the chicken-sdl2 examples:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A surfslice represents a rectangular portion ("slice") of a
;;; surface. This is useful for sprite sheets, which have many
;;; individual images arranged within a single image.

(module surfslice
    (make-surfslice
     surfslice?
     surfslice-surface
     surfslice-rect
     surfslice-w
     surfslice-h

     blit-surfslice!)


  (import scheme)
  (cond-expand
    (chicken-4
     (import chicken)
     (use (prefix sdl2 sdl2:)
          (only data-structures o)
          extras))
    (chicken-5
     (import (chicken base)
             (chicken format)
             (prefix sdl2 sdl2:))))


  (define-record-type surfslice
    (make-surfslice surface rect)
    surfslice?
    (surface surfslice-surface (setter surfslice-surface))
    (rect    surfslice-rect    (setter surfslice-rect)))

  (define-record-printer (surfslice x port)
    (fprintf port "#<surfslice surface: ~A rect: ~S>"
             (surfslice-surface x)
             (surfslice-rect x)))


  (define surfslice-w (o sdl2:rect-w surfslice-rect))
  (define surfslice-h (o sdl2:rect-h surfslice-rect))


  (define (blit-surfslice! slice dst-surf dst-rect)
    (sdl2:blit-surface! (surfslice-surface slice)
                        (surfslice-rect slice)
                        dst-surf
                        dst-rect))

  ) ;; end module
