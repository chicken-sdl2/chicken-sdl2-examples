;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines procedures related to handling user input.
;;; Keyboard (and in the future possibly mouse and joystick) events
;;; are converted into abstract user input actions, according to the
;;; settings in controls.txt. These abstract user input actions are
;;; then processed to trigger a specific response.


;;; List of all valid user input actions. Some of these are not
;;; actually used yet.
(define +user-input-actions+
  '( ;; General user input actions:
    pause
    screenshot

    ;; Player-related user input actions:
    p1-up   p1-left   p1-right
    p2-up   p2-left   p2-right
    p3-up   p3-left   p3-right
    p4-up   p4-left   p4-right
    p5-up   p5-left   p5-right
    ))


;;; This list defines the relationship between player-related user
;;; input action symbols (see list above), and the specific
;;; player-event actions (see player-event.scm).
;;;
;;; Each mapping is a list of this form:
;;;
;;;   (UIACTION  PLAYER  [start: START] [stop: STOP])
;;;
;;; UIACTION is a user input action symbol. PLAYER is a symbol
;;; indicating the player it affects. START and STOP are player-event
;;; actions to trigger when the UIACTION starts or stops (e.g. key
;;; pressed or released). You may omit START and/or STOP.
;;;
(define +input->player-mappings+
  '((p1-up    player1 start: start-up    stop: stop-up)
    (p1-left  player1 start: start-left  stop: stop-left)
    (p1-right player1 start: start-right stop: stop-right)

    (p2-up    player2 start: start-up    stop: stop-up)
    (p2-left  player2 start: start-left  stop: stop-left)
    (p2-right player2 start: start-right stop: stop-right)

    (p3-up    player3 start: start-up    stop: stop-up)
    (p3-left  player3 start: start-left  stop: stop-left)
    (p3-right player3 start: start-right stop: stop-right)

    (p4-up    player4 start: start-up    stop: stop-up)
    (p4-left  player4 start: start-left  stop: stop-left)
    (p4-right player4 start: start-right stop: stop-right)

    (p5-up    player5 start: start-up    stop: stop-up)
    (p5-left  player5 start: start-left  stop: stop-left)
    (p5-right player5 start: start-right stop: stop-right)
    ))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROCESS-USER-INPUT-ACTION

;;; process-user-input-action performs the appropriate steps to
;;; implement the given action. For player-related actions, that means
;;; posting an appropriate player-event to the event queue.
;;;
;;; action is a
;;; state is a boolean indicating whether the action started or
;;; stopped.
;;;
(define (process-user-input-action action state)
  (unless (memq action +user-input-actions+)
    (error "Unrecognized user input action" action))

  ;; If it is any of the player-related actions, pass it to
  ;; process-user-input-player-action for further processing.
  (if (assq action +input->player-mappings+)
      (process-user-input-player-action action state)
      ;; Otherwise, process it here.
      (case action
        ;; (No other actions are implemented yet.)
        (else
         (fprintf (current-error-port)
           "Warning: Unhandled user input action: ~A" action)))))


(define (process-user-input-player-action action state)
  (let* ((mapping (assq action +input->player-mappings+))
         (player  (second mapping))
         (start   (plist-ref #:start mapping))
         (stop    (plist-ref #:stop  mapping)))
    (cond
     ((and start state)
      (sdl2:push-event! (make-player-event player start)))
     ((and stop (not state))
      (sdl2:push-event! (make-player-event player stop))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCANCODE ACTION EVENT HANDLING

;;; Create a handler procedure that looks for the given scancode to be
;;; pressed or released, and processes the action if so. action must
;;; be a symbol in +user-input-actions+, above.
(define (make-scancode-action-event-handler scancode action)
  (lambda (ev) (%handle-scancode-action-event ev scancode action)))

;;; This is a separate procedure so it can be compiled and reused by
;;; many handlers.
(define (%handle-scancode-action-event ev scancode action)
  (if (and (sdl2:keyboard-event? ev)
           (eq? scancode (sdl2:keyboard-event-scancode ev)))
      (begin
        (cond
         ;; If key was pressed (not repeat) process with state #t.
         ((and (sdl2:keyboard-event-state ev)
               (zero? (sdl2:keyboard-event-repeat ev)))
          (process-user-input-action action #t))
         ;; If key was released, process with state #f.
         ((not (sdl2:keyboard-event-state ev))
          (process-user-input-action action #f)))
        ;; The scancode matched, so return #t to indicate that the
        ;; event was consumed.
        #t)
      ;; "This is not the event you are looking for."
      ;; Return #f to indicate that the event was not consumed.
      #f))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAKE CONTROL HANDLER

;;; Create a handler procedure from the given settings. control-plist
;;; should be a plist like those found in controls.txt.
(define (make-control-handler control-plist)
  (validate-control-plist! control-plist)
  (let ((action   (plist-ref '#:action   control-plist))
        (scancode (plist-ref '#:scancode control-plist)))
    (cond
     (scancode
      (make-scancode-action-event-handler scancode action))
     (else
      ;; Somehow an invalid control-plist snuck past the validator.
      (error "Invalid input setting" control-plist)))))


;;; Signals an error if the given control plist is invalid.
(define (validate-control-plist! control-plist)
  (let ((action   (plist-ref '#:action   control-plist))
        (scancode (plist-ref '#:scancode control-plist)))
    (unless action
      (error "Item has no action" control-plist))
    (unless (assq action +input->player-mappings+)
      (error "Item has unrecognized action" control-plist))
    (unless scancode
      (error "Item has no scancode" control-plist))
    (unless (symbol? scancode)
      (error "Item has invalid scancode" control-plist))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOAD CONTROLS

;;; The user controls are loaded from controls.txt. The list of
;;; s-expressions in that file are then converted into a list of event
;;; handler procedures, which can be used with handle-event!.

(define (load-controls)
  (with-error-context "While loading \"controls.txt\" ..."
    (map make-control-handler
         (read-file "controls.txt"))))

(define *controls-handlers*
  (make-parameter (load-controls)))
