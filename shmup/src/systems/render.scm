;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define +draw-entity-bboxes+ #t)


(define (render-scene! scene ren)
  (draw-sky! (scene-sky scene) ren)
  (render-entities! scene ren)
  (sdl2:render-present! ren))


(define (render-entities! scene ren)
  (ecs:for-each-matching
   (scene-db scene) '(texture bbox)
   (lambda (entity texture bbox)
     (render-entity! ren texture bbox))))

(define (render-entity! ren texture bbox)
  (let ((bbox (w->s/r bbox)))
    (sdl2:render-copy! ren texture #f bbox)
    (when +draw-entity-bboxes+
      (sdl2:render-draw-color-set! ren (C 0 255 0))
      (sdl2:render-draw-rect! ren bbox))))
