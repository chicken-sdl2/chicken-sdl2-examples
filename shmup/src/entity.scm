;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; An entity is a moving object in a scene, such as a ship, missile,
;;; or powerup item.

(define-record-type entity
  (%make-entity id type bbox vel scale
                updater collider props)
  entity?
  ;; symbol for finding entity, or #f if not needed
  (id       entity-id       (setter entity-id))
  ;; symbol indicating what kind of entity it is
  (type     entity-type     (setter entity-type))
  ;; HACK: This slot is not used, but it cannot be removed because
  ;; doing so triggers a weird bug.
  (foo      %entity-foo)
  ;; sdl2:rect defining the entity's bounding box
  (bbox     entity-bbox     (setter entity-bbox))
  ;; sdl2:point defining entity's velocity
  (vel      entity-vel      (setter entity-vel))
  ;; real number defining entity's size multiplier
  (scale    entity-scale    (setter entity-scale))
  ;; procedure called each game tick
  (updater  entity-updater  (setter entity-updater))
  ;; procedure called when collide with another entity
  (collider entity-collider (setter entity-collider))
  ;; alist of extra properties for gameplay logic
  (props    entity-props    (setter entity-props)))

(define-type entity (struct entity))


(define (entity-pos entity)
  (rect-center (entity-bbox entity)))


(define (make-entity #!key
                     id type texture
                     (pos (sdl2:make-point))
                     (vel (sdl2:make-point))
                     (scale 0.5)
                     (updater entity-basic-updater)
                     (collider void)
                     (props '()))
  (assert (sdl2:texture? texture))
  (let ((bbox (bbox-for-entity texture scale pos)))
    ;; For now, this returns a list with the entity as the first item,
    ;; and an alist of components as the second item. In the future
    ;; more of the entity's slots will be converted to components,
    ;; until eventually entities will just be ID symbols.
    (list (%make-entity id type bbox vel scale
                        updater collider props)
          (cons* (cons 'texture texture)
                 (cons 'bbox    bbox)
                 (cons 'vel     vel)
                 props))))

(define (bbox-for-entity texture scale center)
  (centered-rect
   (sdl2:point-x center)
   (sdl2:point-y center)
   (* scale (s->w (sdl2:texture-w texture)))
   (* scale (s->w (sdl2:texture-h texture)))))

(compose-accessors entity-pos-x (sdl2:point-x entity-pos))
(compose-accessors entity-pos-y (sdl2:point-y entity-pos))
(compose-accessors entity-vel-x (sdl2:point-x entity-vel))
(compose-accessors entity-vel-y (sdl2:point-y entity-vel))


(define-record-printer (entity e out)
  (fprintf out
    "#<entity ~S bbox: ~S>"
    (entity-id e)
    (sdl2:rect->list (entity-bbox e))))


(define (destroy-entity! entity)
  (scene-remove-entity! (*scene*) entity))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ENTITY PROPS (PROPERTIES)

;;; Get a named property of the entity. Returns default if the
;;; property is not found in the entity.
(: entity-prop
   (entity symbol #!optional * -> *))
(define (entity-prop entity name #!optional default)
  (alist-ref name (entity-props entity) eqv? default))

;;; Set a property of the entity.
(: entity-prop-set!
   (entity symbol * -> void))
(define (entity-prop-set! entity name value)
  (set! (entity-props entity)
        (alist-update! name value (entity-props entity))))

(set! (setter entity-prop) entity-prop-set!)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UPDATE ENTITY

;;; Update the entity. dt is delta time, the number of seconds that
;;; have passed since the last update. This procedure just calls the
;;; entity's updater procedure.
(define (update-entity! entity dt)
  ((entity-updater entity) entity dt))


;;; The default entity updater procedure.
(define (entity-basic-updater entity dt)
  (update-entity-pos! entity dt))


;;; Update entity's position based on its current velocity.
(define (update-entity-pos! entity dt)
  (sdl2:rect-add-point!
   (entity-bbox entity)
   (sdl2:point-scale (entity-vel entity) (exact->inexact dt))))


;;; Returns #t if any of entity's bbox is touching scene's bounds.
(define (entity-in-scene-bounds? entity scene)
  (sdl2:has-intersection?
   (entity-bbox entity)
   (scene-bounds scene)))


;;; Move the entity so its bbox is completely inside rect.
(define (entity-clamp-to-rect! entity rect)
  (receive (dx dy) (need-clamp-rect (entity-bbox entity) rect)
    (sdl2:rect-move! (entity-bbox entity) dx dy)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COLLISION

(define (entity-collision! entity other)
  ((entity-collider entity) entity other))
