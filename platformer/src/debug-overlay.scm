;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines parameters and procedures related to drawing a
;;; debug overlay, which visualizes various things to help debug.


(define (need-draw-debug-overlay?)
  (and (get-setting 'debug-overlay?)
       (or (get-setting 'draw-entity-bboxes?)
           (get-setting 'draw-entity-hitboxes?)
           (get-setting 'draw-bpcd-tiles?))))

(define (draw-debug-overlay! scene dst)
  (when (get-setting 'draw-entity-bboxes?)
    (draw-entity-bboxes! scene dst))
  (when (get-setting 'draw-entity-hitboxes?)
    (draw-entity-hitboxes! scene dst))
  (when (get-setting 'draw-bpcd-tiles?)
    (draw-bpcd-tiles! scene dst)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VISUALIZATIONS

(define (draw-entity-bboxes! scene dst)
  (for-each
   (lambda (entity)
     (draw-outlined-rect!
      dst (w->s/r (entity-bbox entity))
      color: '(255 0 0) alpha: 128))
   (scene-entities scene)))


(define (draw-entity-hitboxes! scene dst)
  (for-each
   (lambda (entity)
     (for-each
      (lambda (hitbox)
        (draw-outlined-rect!
         dst (w->s/r hitbox)
         color: '(0 220 0) alpha: 180))
      (map cdr (entity-hitboxes entity))))
   (scene-entities scene)))


(define (draw-bpcd-tiles! scene dst)
  (for-each
   (lambda (entity)
     (for-each
      (lambda (tile)
        (draw-filled-rect!
         dst (w->s/r (tile-bbox tile))
         blend: 'add color: '(0 255 0) alpha: 50))
      (nearby-solid-tiles entity (scene-level scene))))
   (scene-entities scene)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DEBUG OVERLAY HIGHLIGHT

;;; A surface containing a single white pixel. This is used (with
;;; different blend modes, alpha mods, and color mods) for drawing
;;; highlights and lines on the debug overlay.
(define *debug-overlay-pixel*
  (make-parameter
   (let ((surf (sdl2:make-surface 1 1 16)))
     (sdl2:fill-rect! surf #f (sdl2:make-color 255 255 255))
     surf)))

(define (draw-filled-rect! dst rect #!key
                           (blend 'blend) (alpha 255)
                           (color '(255 255 255)))
  (let ((surf (*debug-overlay-pixel*)))
    (set! (sdl2:surface-blend-mode surf) blend)
    (set! (sdl2:surface-alpha-mod  surf) alpha)
    (set! (sdl2:surface-color-mod  surf) color)
    (sdl2:blit-scaled! surf #f dst rect)))

;;; Draw the outlines of the given rect onto the destination surface,
;;; using the given blend mode, alpha mod, and color mod.
(define (draw-outlined-rect! dst rect #!key
                             (blend 'blend) (alpha 255)
                             (color '(255 255 255))
                             (thick 1))
  (let ((surf (*debug-overlay-pixel*))
        (t    thick)
        (t2   (floor (/ thick 2)))
        (-t2  (floor (/ thick -2))))
    (receive (x y w h) (apply values (sdl2:rect->list rect))
      (set! (sdl2:surface-blend-mode surf) blend)
      (set! (sdl2:surface-alpha-mod  surf) alpha)
      (set! (sdl2:surface-color-mod  surf) color)
      (for-each
       (cut sdl2:blit-scaled! surf #f dst <>)
       ;; Left, right, top, and bottom lines
       (list (R (- x t2)    (- y t2)    t           (+ h t2 t2))
             (R (+ x w -t2) (- y t2)    t           (+ h t2 t2))
             (R (- x t2)    (- y t2)    (+ w t2 t2) t)
             (R (- x t2)    (+ y h -t2) (+ w t2 t2) t))))))
