;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define (make-bouncer #!key id
                      (pos (P 0 0))
                      (vel (P 0 0))
                      (scale 0.5)
                      (props '()))
  (make-entity
   id:      id
   type:    'enemy
   updater: bouncer-updater
   texture: (get-texture 'enemy-black4)
   pos:     pos
   vel:     vel
   scale:   scale
   props:   (cons '(subtype . bouncer) props)))


(define (bouncer-updater enemy dt)
  (update-entity-pos! enemy dt)
  ;; If enemy touched edge of scene, then change velocity.
  (receive (dx dy) (need-clamp-rect (entity-bbox enemy)
                                    (scene-bounds (*scene*)))
    (unless (zero? dx)
      (set! (sdl2:point-x (entity-vel enemy))
        (* (signum dx) (abs (sdl2:point-x (entity-vel enemy))))))
    (unless (zero? dy)
      (set! (sdl2:point-y (entity-vel enemy))
        (* (signum dy) (abs (sdl2:point-y (entity-vel enemy))))))))
