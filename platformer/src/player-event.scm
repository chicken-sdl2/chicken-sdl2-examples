;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines the player event type. This event type is used
;;; when certain player-related actions occur. It is a custom kind of
;;; sdl2:user-event, with the type symbols 'player1 through 'player5.
;;; It has three fields:
;;;
;;; - action: Symbol indicating what happened (see list below).
;;; - value1: Extra integer data used with certain actions.
;;; - value2: Extra integer data used with certain actions.
;;;
;;; The purpose of the player event type is to abstract the possible
;;; actions that can happen for a player, so that the game logic is
;;; loosely coupled. For example, the keyboard handling code does not
;;; need to know how to make a player move left. Instead, it can
;;; simply push a 'player-start-left event to the event queue. Then
;;; the player event handling code will see the event and make the
;;; player start moving left.
;;;
;;; There are "start" and "stop" actions for the four directions:
;;; "up", "down", "left", and "right". These represent abstract user
;;; input, not the keyboard keys with those names. For example,
;;; depending on the settings, pressing the D key might cause a
;;; 'player-start-right event to occur for player 1. When the D key is
;;; released, a corresponding 'player-stop-right event would occur.
;;; Or, it might be a joystick axis, button, etc. that triggers the
;;; events for a certain player.


;;; A list of player event types. There is one event type for each
;;; possible player in the game. They match the entity IDs of the
;;; players, to make it easy to figure out who the event belongs to.
(define +player-event-types+
  '(player1
    player2
    player3
    player4
    player5))


;;; An a list of all the possible player event actions. Some of them
;;; are not actually used (yet). sdl2:user-event can only hold
;;; integers, so we need to convert back and forth.
(define +player-event-actions+
  (map cons
       (iota 100)
       '(start-up                ; started "up" input
         stop-up                 ; stopped "up" input
         ;; start-down           ; started "down" input
         ;; stop-down            ; stopped "down" input
         start-left              ; started "left" input
         stop-left               ; stopped "left" input
         start-right             ; started "right" input
         stop-right              ; stopped "right" input
         spawned                 ; spawned or respawned
         need-respawn            ; e.g. died or fell off level
         ;; start-walk-left      ; started walking left
         ;; stop-walk-left       ; stopped walking left
         ;; start-walk-righ      ; started walking right
         ;; stop-walk-right      ; stopped walking right
         jumped                  ; initiated a jump
         fell                    ; fell or walked off tile w/o jumping
         landed                  ; feet touched tile. values = col/row
         got-treasure            ; picked up coin/gem. value1 = points
         )))


(sdl2:register-events! +player-event-types+)

(define (player-event-type? type)
  (memq type +player-event-types+))


(define (player-event? ev)
  (and (sdl2:event? ev) (player-event-type? (sdl2:event-type ev))))


(: make-player-event
   (symbol symbol #!optional fixnum fixnum
    -> (struct sdl2:event)))
(define (make-player-event type action #!optional (value1 0) (value2 0))
  (assert (player-event-type? type)
          (sprintf "Invalid player event type: ~S" type))
  (let ((ev (sdl2:make-event type)))
    (player-event-action-set! ev action)
    (player-event-value1-set! ev value1)
    (player-event-value2-set! ev value2)
    ev))


(define (print-player-event ev #!optional (out (current-output-port)))
  (fprintf out
    "#<player-event ~S action: ~S value1: ~S value2: ~S>"
    (sdl2:event-type ev)
    (player-event-action ev)
    (player-event-value1 ev)
    (player-event-value2 ev)))

(define (sprint-player-event ev)
  (with-output-to-string (cut print-player-event ev)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ACTION

;;; Return the player event action symbol for the event.
;;; See the list at the top of this file.
(: player-event-action ((struct sdl2:event) -> symbol))
(define (player-event-action ev)
  (assert (player-event? ev))
  (%int->player-event-action
   (sdl2:user-event-code ev)))

;;; Set the player event action symbol for the event.
(: player-event-action-set! ((struct sdl2:event) symbol -> void))
(define (player-event-action-set! ev action)
  (assert (player-event? ev))
  (sdl2:user-event-code-set!
   ev (%player-event-action->int action)))

(set! (setter player-event-action) player-event-action-set!)


(: %player-event-action->int (symbol -> fixnum))
(define (%player-event-action->int sym)
  (let ((pair (rassoc sym +player-event-actions+)))
    (if pair
        (car pair)
        (error "Invalid player-event-action symbol" sym))))

(: %int->player-event-action (fixnum -> symbol))
(define (%int->player-event-action int)
  (let ((pair (assoc int +player-event-actions+)))
    (if pair
        (cdr pair)
        (error "Invalid player-event-action int" sym))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VALUE1

;;; Return the value1 integer for the event. This is only used for
;;; certain actions:
;;;
;;; - landed:  value1 indicates the grid column of the tile.
;;; - got-treasure:  value1 indicates the worth of the coin or gem,
;;;   i.e. how many points the player gains from picking it up.
;;;
;;; Under the hood, the number is stored as a pointer address in the
;;; event's data1 slot.
(: player-event-value1 ((struct sdl2:event) -> fixnum))
(define (player-event-value1 ev)
  (assert (player-event? ev))
  (let ((data (sdl2:user-event-data1-raw ev)))
    (if (pointer? data)
        (pointer->address data)
        0)))

(: player-event-value1-set! ((struct sdl2:event) fixnum -> void))
(define (player-event-value1-set! ev num)
  (assert (player-event? ev))
  (sdl2:user-event-data1-raw-set! ev (address->pointer num)))

(set! (setter player-event-value1) player-event-value1-set!)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VALUE2

;;; Return the value2 integer for the event. This is only used for
;;; certain actions:
;;;
;;; - landed:  value2 indicates the grid row of the tile.
;;;
;;; Under the hood, the number is stored as a pointer address in the
;;; event's data2 slot.
(: player-event-value2 ((struct sdl2:event) -> fixnum))
(define (player-event-value2 ev)
  (assert (player-event? ev))
  (let ((data (sdl2:user-event-data2-raw ev)))
    (if (pointer? data)
        (pointer->address data)
        0)))

(: player-event-value2-set! ((struct sdl2:event) fixnum -> void))
(define (player-event-value2-set! ev num)
  (assert (player-event? ev))
  (sdl2:user-event-data2-raw-set! ev (address->pointer num)))

(set! (setter player-event-value2) player-event-value2-set!)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; HANDLING

(define (player-event-handler ev)
  (if (not (player-event? ev))
      +event-not-consumed+
      (begin
        (when (get-setting 'print-player-events)
          (print-player-event ev)
          (newline))
        (process-player-event ev))))


(define (process-player-event ev)
  (let ((player (scene-find-entity (*scene*) (sdl2:event-type ev))))
    (if (not player)
        (sprintf "Warning: No player found to match event: ~S"
                 (sprint-player-event ev))

        (case (player-event-action ev)
          ((start-up)
           (player-start-up! player)
           +event-consumed+)
          ((stop-up)
           (player-stop-up! player)
           +event-consumed+)

          ;; Currently unused.
          ;; ((start-down) +event-consumed+)
          ;; ((stop-down)  +event-consumed+)

          ((start-left)
           (player-start-left! player)
           +event-consumed+)
          ((stop-left)
           (player-stop-left! player)
           +event-consumed+)

          ((start-right)
           (player-start-right! player)
           +event-consumed+)
          ((stop-right)
           (player-stop-right! player)
           +event-consumed+)

          ;; Currently unused.
          ;; ((start-walk-left) +event-consumed+)
          ;; ((stop-walk-left) +event-consumed+)
          ;; ((start-walk-right) +event-consumed+)
          ;; ((stop-walk-right) +event-consumed+)

          ((need-respawn)
           (scene-respawn-player! (*scene*) player)
           +event-consumed+)

          ;; We don't consume these. They are to let other systems
          ;; know that something happened.
          ((spawned)      +event-not-consumed+)
          ((jumped)       +event-not-consumed+)
          ((fell)         +event-not-consumed+)
          ((landed)       +event-not-consumed+)
          ((got-treasure) +event-not-consumed+)

          (else
           (sprintf "Warning: Unhandled action: ~S"
                    (sprint-player-event ev))
           +event-not-consumed+)))))
