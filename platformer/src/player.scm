;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file contains procedures related to player entities.


(define (make-player id color)
  (make-entity id 'player (player-sprite color)
               (make-body) (make-player-shape) '()
               `((color . ,color))))

(define (player? x)
  (and (entity? x) (eq? 'player (entity-type x))))


(define (player-sprite color)
  (case color
    ((green)  'player-green-front)
    ((blue)   'player-blue-front)
    ((pink)   'player-pink-front)
    ((yellow) 'player-yellow-front)
    ((beige)  'player-beige-front)))


(define +player-possible-ids+
  '(player1
    player2
    player3
    player4
    player5))

(define +player-default-colors+
  '((player1 . pink)
    (player2 . green)
    (player3 . yellow)
    (player4 . beige)
    (player5 . blue)))


;;; Returns tile label for primary spawn point of given player.
;;; Players prefer to spawn at a primary spawn point at the start of
;;; the game.
(define (player-primary-spawn-label player)
  (alist-ref (entity-id player) +player-primary-spawn-labels+))


(define +player-primary-spawn-labels+
  '((player1 . pl1)
    (player2 . pl2)
    (player3 . pl3)
    (player4 . pl4)
    (player5 . pl5)))

;;; Any player can spawn at a secondary spawn point.
(define +player-secondary-spawn-label+ 'pl*)

(define +player-spawn-labels+
  (cons +player-secondary-spawn-label+
        (map cdr +player-primary-spawn-labels+)))


(define make-player-shape
  (let* ((w    (s->w 18))
         (h    (s->w (- +grid-size+ 1))) ; so they can fit in gaps
         (w*   (- w (s->w 4)))
         (h*   (- h (s->w 8)))
         (w*/2 (fx/ w* 2))
         (h*/2 (fx/ h* 2))
         (w/2  (fx/ w  2))
         (h/2  (fx/ h  2)))
    (lambda ()
      (make-cshape
       ;; NOTE: Hitbox order affects precedence during resolution.
       `((bottom . ,(R  (- w*/2)  0         w*   h/2))
         (top    . ,(R  (- w*/2)  (- h/2)   w*   h/2))
         (left   . ,(R  (- w/2)   (- h*/2)  w/2  h*))
         (right  . ,(R  0         (- h*/2)  w/2  h*)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MOVEMENT CONSTANTS

;;; Velocity change when jumping
(define +player-jump-push+ (s->w 280))

;;; Velocity change when starting to walk
(define +player-walk-push+ (s->w 40))

;;; Acceleration while holding left/right on ground or in air
(define +player-horiz-ground-accel+ (s->w 600))
(define +player-horiz-air-accel+ (s->w 300))

;;; Maximum x velocity (positive or negative)
(define +player-max-vx+ (s->w 100))

;;; Base friction coefficient
(define +player-base-friction+ 6)

;;; Multiply horizontal friction by this much when player is walking
(define +player-walk-friction-scale+ 0.5)

;;; How many seconds player can be colling with no tiles before they
;;; are considered to be falling.
(define +player-max-fall-timer+ 0.01)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROPERTY SHORTCUTS

(define-entity-prop player-color          'color)
(define-entity-prop player-score          'score          0)
(define-entity-prop player-holding-up?    'holding-up?    #f)
(define-entity-prop player-holding-left?  'holding-left?  #f)
(define-entity-prop player-holding-right? 'holding-right? #f)
(define-entity-prop player-will-respawn?  'will-respawn?  #f)
(define-entity-prop player-jumping?       'jumping?       #f)
(define-entity-prop player-falling?       'falling?       #t)

;;; Increases while player is not touching a tile, unless they
;;; actively jumped. Once it reaches +player-max-fall-timer+, the
;;; player is considered to be falling.
(define-entity-prop player-fall-timer     'fall-timer     0)

;;; Friction scale from the tile the player is standing on. 1.0 is
;;; normal friction, 0.5 is half friction, 0.0 is no friction, etc.
(define-entity-prop player-friction       'friction       0)

(define (player-in-air? player)
  (or (player-jumping? player)
      (player-falling? player)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COLLISION RESOLUTION

;;; Resolve the given collisions between player and tile. collisions
;;; is a list of (symbol . rect) pairs indicating one of the player's
;;; hitbox that collided with the tile. (Such a list is returned by
;;; check-entity-tile-collisions.)
(define (player-resolve-e/t-colls!
         player tile collisions dt)
  (if (null? collisions)
      ;; Not colliding with any tiles.
      (player-no-e/t-coll! player dt)

      ;; Resolve only the first collision with each tile. This way, if
      ;; the player falls deep into a tile, only the bottom collision
      ;; will matter (because it has precedence), so the player won't
      ;; be moved horizontally.
      (player-resolve-e/t-coll! player tile (tile-hitbox tile)
                                (first collisions) dt)))


;;; Called when the player is not colliding with any tile.
(define (player-no-e/t-coll! player dt)
  ;;; Fall timer is commented out until it works better.
  ;; ;; Increase the player's fall timer unless already in the air.
  ;; (unless (player-in-air? player)
  ;;   (inc! (player-fall-timer player) dt)
  ;;   ;; If the timer is full, they are officially falling.
  ;;   (when (>= (player-fall-timer player) +player-max-fall-timer+)
  ;;     (player-fall! player)))
  (void))


;;; Resolve the single given collision between player and tile.
;;; collision is a (symbol . rect) indicating one of the player's
;;; hitbox that collided with the tile.
(define (player-resolve-e/t-coll! player tile tile-hb collision dt)
  (let ((hb-label (car collision))
        (hb-rect  (cdr collision)))
    (case hb-label
      ((bottom)
       ;; Cancel downward velocity.
       (set! (sdl2:point-y (entity-v player))
         (min 0 (sdl2:point-y (entity-v player))))
       ;; Move player so they are above the tile.
       (let ((dy (- (sdl2:rect-y tile-hb) (rect-bottom hb-rect))))
         (sdl2:point-add! (entity-p player) (P 0 dy)))
       ;; If the player was in the air, now their feet have touched
       ;; this tile, so signal that they have landed.
       (when (player-in-air? player)
         (player-land! player tile)))

      ((top)
       ;; Cancel upward velocity.
       (set! (sdl2:point-y (entity-v player))
         (max 0 (sdl2:point-y (entity-v player))))
       ;; Move player so they are below the tile.
       (let ((dy (- (rect-bottom tile-hb) (sdl2:rect-y hb-rect))))
         (sdl2:point-add! (entity-p player) (P 0 dy))))

      ((left)
       ;; Cancel leftward velocity.
       (set! (sdl2:point-x (entity-v player))
         (max 0 (sdl2:point-x (entity-v player))))
       ;; Move player so they are right of the tile.
       (let ((dx (- (rect-right tile-hb) (sdl2:rect-x hb-rect))))
         (sdl2:point-add! (entity-p player) (P dx 0))))

      ((right)
       ;; Cancel rightward velocity.
       (set! (sdl2:point-x (entity-v player))
         (min 0 (sdl2:point-x (entity-v player))))
       ;; Move player so they are left of the tile.
       (let ((dx (- (sdl2:rect-x tile-hb) (rect-right hb-rect))))
         (sdl2:point-add! (entity-p player) (P dx 0)))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PLAYER ACTIONS


(define (player-start-up! player)
  (set! (player-holding-up? player) #t)
  ;; If the player presses up while not in the air, they jump.
  (unless (player-in-air? player)
    (player-jump! player))
  (update-player-accel! player))

(define (player-stop-up! player)
  (set! (player-holding-up? player) #f)
  (update-player-accel! player))



(define (player-start-left! player)
  (set! (player-holding-left? player) #t)

  ;; Give the player a leftward push to get them started.
  (unless (player-in-air? player)
    (dec! (entity-vx player)
          (* (player-friction player)
             +player-walk-push+)))

  (update-player-accel! player))

(define (player-stop-left! player)
  (set! (player-holding-left? player) #f)
  (update-player-accel! player))



(define (player-start-right! player)
  (set! (player-holding-right? player) #t)

  ;; Give the player a rightward push to get them started.
  (unless (player-in-air? player)
    (inc! (entity-vx player)
          (* (player-friction player)
             +player-walk-push+)))

  (update-player-accel! player))

(define (player-stop-right! player)
  (set! (player-holding-right? player) #f)
  (update-player-accel! player))



;;; This is called when the player initiates a jump.
(define (player-jump! player)
  ;; Update player's properties.
  (set! (player-jumping? player) #t)
  (set! (player-falling? player) #f)
  (set! (player-friction player) 0)
  (update-player-accel! player)

  ;; Nudge player upward so they are no longer touching tile below.
  ;; Otherwise physics engine would think they landed immediately.
  (dec! (entity-py player) (s->w 2))

  ;; Give the player some upward velocity.
  (dec! (entity-vy player) +player-jump-push+)

  ;; Push an event to notify that the player has jumped.
  (sdl2:push-event!
   (make-player-event (entity-id player) 'jumped)))



;;; This is called when the player walks off a tile, is pushed, or
;;; falls without initiating a jump.
(define (player-fall! player)
  ;; Update player's properties.
  (set! (player-falling? player) #t)
  (set! (player-jumping? player) #f)
  (set! (player-friction player) 0)
  (set! (player-fall-timer player) 0)
  (update-player-accel! player)

  ;; Push an event to notify that the player has fallen.
  (sdl2:push-event!
   (make-player-event (entity-id player) 'fell)))



;;; This is called when the player's feet touch a solid tile.
(define (player-land! player tile)
  ;; Update player's properties.
  (set! (player-jumping? player) #f)
  (set! (player-falling? player) #f)
  (set! (player-fall-timer player) 0)
  (set! (player-friction player) (tile-prop tile 'friction 1))
  (update-player-accel! player)

  ;; Push an event to notify that the player has landed.
  (sdl2:push-event!
   (make-player-event (entity-id player) 'landed
                      (tile-col tile) (tile-row tile))))



(define (player-out-of-bounds! player)
  ;; Don't need to do anything if player is already about to respawn.
  (unless (player-will-respawn? player)
    (set! (player-will-respawn? player) #t)
    ;; Push an event to notify that the player needs respawn.
    (sdl2:push-event!
     (make-player-event (entity-id player) 'need-respawn))))


(define (player-respawn! player tile)
  (entity-move-to-tile! player tile)
  (set! (entity-vx player) 0)
  (set! (entity-vy player) 0)

  ;; Update player's properties.
  (set! (player-will-respawn? player) #f)
  (set! (player-jumping? player) #f)
  (set! (player-falling? player) #t)
  (update-player-accel! player))



(define (player-got-treasure! player value)
  (inc! (player-score player) value)
  ;; Push an event to notify that the player got treasure.
  (sdl2:push-event!
   (make-player-event (entity-id player) 'got-treasure value)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UPDATE PLAYER

(define (update-player! player dt)
  ;; Apply friction if needed
  (unless (zero? (player-friction player))
    (apply-player-friction! player dt))

  ;; Limit player's horizontal velocity
  (let ((vx (entity-vx player)))
    (when (> (abs vx) +player-max-vx+)
      (if (positive? vx)
          (set! (entity-vx player) +player-max-vx+)
          (set! (entity-vx player) (- +player-max-vx+)))))

  ;; Perform normal entity update (e.g. updating body)
  (update-entity! player dt))


(define (update-player-accel! player)
  (let ((dir (player-holding-horiz player))
        (mag (if (player-in-air? player)
                 +player-horiz-air-accel+
                 (* (max 0.5 (player-friction player))
                    +player-horiz-ground-accel+))))
    (entity-force-set! player 'horiz (P (* dir mag) 0))))


(define (apply-player-friction! player dt)
  (let ((scale (* dt +player-base-friction+
                  (player-friction player)
                  (if (and (not (player-in-air? player))
                           (player-holding-with-vx? player))
                      +player-walk-friction-scale+
                      1))))
    (dec! (entity-vx player)
          (round (* scale (entity-vx player))))))


;;; Returns -1 if the player is holding left, 1 if the player is
;;; holding right, or 0 if holding neither or both.
(define (player-holding-horiz player)
  (let ((left (player-holding-left? player))
        (right (player-holding-right? player)))
    (cond ((and left right) 0)
          (left            -1)
          (right            1)
          (else             0))))

;;; Returns true if the player is holding in the same direction as
;;; their current horizontal velocity. I.e. holding left while moving
;;; left, or holding right while moving right.
(define (player-holding-with-vx? player)
  (let ((dir (player-holding-horiz player))
        (vx (entity-vx player)))
    (or (and (negative? dir) (negative? vx))
        (and (positive? dir) (positive? vx)))))
