;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A tiletype describes a type of tile. Many tiles can have the same
;;; tiletype. The tiletype specifies the appearance and default
;;; properties of a tile.
;;;
;;; A tiletype has a three-character symbol id which is used in the
;;; level file format to refer to this tiletype. The id symbol must be
;;; unique among all tiletypes on a level. Usually the three
;;; characters of the id represent increasing levels of specificity.
;;; For example, id TGL might mean "Terrain / Grass / Left". If the
;;; tiletype does not need all three characters to uniquely identify
;;; itself, the remaining characters should be "-". For example, id
;;; PS- might mean "Prop / Spikes". By convention, id --- means empty
;;; space.
;;;
;;; A tiletype's appearance is determined by a list of sprite symbols.
;;; The sprites are blitted in order, which means the earlier sprites
;;; will appear behind the later sprites.
;;;
;;; A tiletype have an alist of extra properties which can be used by
;;; the game logic. For example it might have a property indicating
;;; that tiles of this type are nonsolid by default (entities can pass
;;; through them), or that they harm players who touch them. A
;;; property in an individual tile takes precedence over a property of
;;; the same name in the tiletype.


(define-record-type tiletype
  (%make-tiletype id sprites props)
  tiletype?
  (id      tiletype-id      (setter tiletype-id))      ; unique symbol
  (sprites tiletype-sprites (setter tiletype-sprites)) ; sprite names
  (props   tiletype-props   (setter tiletype-props)))  ; properties alist

(define-type tiletype (struct tiletype))


(: make-tiletype
   ((or symbol boolean) (list-of symbol)
    #!optional (list-of (pair symbol *))
    -> tiletype))
(define (make-tiletype id sprites #!optional (props '()))
  (unless (and (symbol? id) (= 3 (string-length (symbol->string id))))
    (error "tiletype id must be symbol with 3 chars" id))
  (%make-tiletype id sprites props))


(define-record-printer (tiletype t out)
  (fprintf out "#<tiletype ~S>" (tiletype-id t)))


;;; Get a property from the tiletype. Returns default if the property
;;; is not found.
(: tiletype-prop
   (tiletype symbol #!optional * -> *))
(define (tiletype-prop tiletype name #!optional default)
  (alist-ref name (tiletype-props tiletype) eqv? default))

;;; Set a property of the tiletype.
(: tiletype-prop-set!
   (tiletype symbol * -> void))
(define (tiletype-prop-set! tiletype name value)
  (set! (tiletype-props tiletype)
        (alist-update! name value (tiletype-props tiletype))))

(set! (setter tiletype-prop) tiletype-prop-set!)


;;; Draw this tile type with its top left corner at the given x/y
;;; coordinates (pixels).
(define (draw-tiletype! tiletype dst x y)
  (let ((rect (R (round x) (round y)
                 +grid-size+ +grid-size+)))
    (for-each (cut blit-sprite! <> dst rect)
              (tiletype-sprites tiletype))))
