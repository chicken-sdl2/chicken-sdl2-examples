;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines procedures related to collision detection
;;; between two entities.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BROAD PHASE ENTITY/ENTITY COLLISION DETECTION.

;;; Returns a list of (entity . entity) pairs for entities whose
;;; bounding boxes are colliding. The list will not have duplicates,
;;; e.g. if it has (a . b) then it won't have (b . a).
(: broad-e/e-colls
   ((list-of entity) -> (list-of (pair entity entity))))
(define (broad-e/e-colls entities)
  (let recur ((entities entities)
              (accum '()))
    (if (null? entities)
        (concatenate! (reverse! accum))
        (recur (cdr entities)
               (cons (%broad-e/e-colls
                      (car entities) (cdr entities))
                     accum)))))

;;; Returns a list of (e . other) pairs for every entity in others
;;; that collides with entity e.
(: %broad-e/e-colls
   (entity (list-of entity) -> (list-of (pair entity entity))))
(define (%broad-e/e-colls e others)
  (filter-map
   (lambda (other)
     (if (entity-bboxes-collide? e other)
         (cons e other)
         #f))
   others))


(: entity-bboxes-collide? (entity entity -> boolean))
(define (entity-bboxes-collide? e1 e2)
  (sdl2:has-intersection?
   (entity-bbox e1) (entity-bbox e2)))
