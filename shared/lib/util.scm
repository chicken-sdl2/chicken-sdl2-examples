;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file contains various utility procedures and macros.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ALIASES

(define C sdl2:make-color)
(define P sdl2:make-point)
(define R sdl2:make-rect)

(define-type rect  (struct sdl2:rect))
(define-type point (struct sdl2:point))
(define-type color (struct sdl2:color))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DATA STRUCTURES

;;; Fetch an item from alist. Signals an error if key is not found.
(define (alist-ref/required key alist #!key
                            (test eqv?)
                            (msg "Required key is missing"))
  (let* ((g (gensym))
         (result (alist-ref key alist test g)))
    (if (eq? g result)
        (error msg key)
        result)))

;;; Fetch several items from alist, returning them as multiple values.
;;; Signals an error if any of the keys is not found.
(define (alist-refs/required keys alist #!key
                             (test eqv?)
                             (msg "Required key is missing"))
  (apply values
    (map (cut alist-ref/required <> alist test: test msg: msg)
         keys)))


;;; Convert plist to an alist. plist must be a proper, non-circular
;;; list with an even number of items.
;;;
;;;   (plist->alist '(a b c d))
;;;   => ((a . b) (c . d))
(define (plist->alist plist)
  (unless (even? (length plist))
    (error 'plist->alist "list has odd length" plist))
  (let recur ((plist plist)
              (accum '()))
    (if (null? plist)
        (reverse! accum)
        (recur (cddr plist)
               (alist-cons (car plist) (cadr plist) accum)))))


;;; Fetch an item from plist.
(define (plist-ref key plist #!key (test eqv?) default)
  (unless (even? (length plist))
    (error 'plist-ref "list has odd length" plist))
  (let recur ((plist plist))
    (cond
     ((null? plist)           default)
     ((test key (car plist))  (cadr plist))
     (else                    (recur (cddr plist))))))

;;; Fetch an item from alist. Signals an error if key is not found.
(define (plist-ref/required key plist #!key
                            (test eqv?)
                            (msg "Required key is missing"))
  (let* ((g (gensym))
         (result (plist-ref key plist test: test default: g)))
    (if (eq? g result)
        (error msg key)
        result)))


;;; Return a random item from the given non-empty list.
(: list-pick-random (pair -> *))
(define (list-pick-random l)
  (assert (not (null? l)))
  (list-ref l (random (length l))))

;;; Return a random item from the given non-empty vector.
(: vector-pick-random (vector -> *))
(define (vector-pick-random v)
  (assert (> (vector-length v) 1))
  (vector-ref v (random (vector-length v))))

;;; Return a copy of the list, shuffled into a random order.
(define (shuffle-list l)
  (define (random-cons item) (cons (random 100) item))
  (define (car<? a b) (< (car a) (car b)))
  (map cdr (sort! (map random-cons l) car<?)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; EXCEPTION HANDLING

;;; If an exception occurs in body, this catches the exception, prints
;;; the context string, then re-signals the exception. Otherwise this
;;; just returns the value of the last body form.
(define-syntax with-error-context
  (syntax-rules ()
    ((with-error-context context-string body ...)
     (condition-case
      (begin body ...)
      (e (exn)
         (fprintf (current-error-port)
           "Error: ~A~%" context-string)
         (abort e))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COMPOSITION

(define-syntax compose-accessors
  (syntax-rules ()
    ((compose-accessors name (accessor1 accessor2))
     (begin
       (define (name item)
         (accessor1 (accessor2 item)))
       (set! (setter name)
         (lambda (object value)
           (set! (accessor1 (accessor2 object)) value)))))))


(define-syntax define-entity-prop
  (syntax-rules ()
    ((define-entity-prop name prop-name)
     (define-entity-prop name prop-name #f))
    ((define-entity-prop name prop-name default-val)
     (begin
       (define (name entity #!optional (default default-val))
         (entity-prop entity prop-name default))
       (set! (setter name)
         (lambda (entity value)
           (set! (entity-prop entity prop-name) value)))))))
