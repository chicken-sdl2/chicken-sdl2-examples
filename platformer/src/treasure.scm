;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define (make-treasure type)
  (make-entity (gensym type) type
               (treasure-sprite type)
               (make-body)
               (make-cshape `((bounds . ,(s->w/r (R -5 -5 10 10)))))
               '() ;; no forces
               '((static . #t))))


(define (treasure? x)
  (and (entity? x)
       (member (entity-type x) +treasure-types+)))


(define (treasure-sprite type)
  (case type
    ((coin-bronze) 'item-coin-bronze)
    ((coin-silver) 'item-coin-silver)
    ((coin-gold)   'item-coin-gold)
    ((gem-yellow)  'item-gem-yellow)
    ((gem-green)   'item-gem-green)
    ((gem-red)     'item-gem-red)
    ((gem-blue)    'item-gem-blue)
    (else (error "Unknown treasure type" type))))


(define (treasure-value treasure)
  (treasure-type-value (entity-type treasure)))

(define (treasure-type-value type)
  (or (alist-ref type +treasure-values+)
      (error "Unknown treasure type" type)))


(define +treasure-values+
  '((coin-bronze . 1)
    (coin-silver . 3)
    (coin-gold   . 5)
    (gem-yellow  . 10)
    (gem-green   . 15)
    (gem-red     . 20)
    (gem-blue    . 30)))

(define +treasure-types+
  (map car +treasure-values+))


;;; Tile labels where treasure will randomly spawn.
(define +treasure-spawn-labels+
  '(tr1 tr2 tr3 tr4 tr5))

;;; This describes the spawning behavior of each of the treasure spawn
;;; tile labels. chance is the percent chance (out of 100) that a tile
;;; will with this label will spawn a treasure if selected by the
;;; algorithm. min-value and max-value describe the minimum and
;;; maximum treasure value that the label can spawn (it will choose a
;;; random treasure between the min and max).
(define +treasure-table+
  '((tr1 (chance    . 30)
         (min-value . 1)
         (max-value . 5))
    (tr2 (chance    . 30)
         (min-value . 1)
         (max-value . 10))
    (tr3 (chance    . 10)
         (min-value . 3)
         (max-value . 15))
    (tr4 (chance    . 10)
         (min-value . 5)
         (max-value . 20))
    (tr5 (chance    . 5)
         (min-value . 15)
         (max-value . 100))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CHOOSING TREASURE SPAWN POINTS
;;;
;;; These procedures are used to choose which treasure spawn points
;;; should spawn a treasure.

;;; Choose up to N tiles from given list of tiles, taking into account
;;; the chance of their treasure spawn label. If given fewer than N
;;; tiles, returns all given tiles in a random order.
(define (choose-treasure-spawns n tiles)
  (define (%tile-with-chance tile)
    (cons tile (treasure-chance-for-tile tile)))

  ;; Perform a random calculation based on the tile's chance. Returns
  ;; #t if the given tile was chosen, or #f if not chosen.
  (define (%choose? tile-with-chance)
    (< (random 100) (cdr tile-with-chance)))

  ;; Recursive loop to look through all the tiles and randomly choose
  ;; or reject them. If we run out of available tiles, the previously
  ;; rejected tiles will be considered again.
  (let recur ((available (shuffle-list
                          (map %tile-with-chance tiles)))
              (chosen    '())
              (rejected  '()))
    (cond
     ;; We have chosen enough tiles, so return them (without chance).
     ((= n (length chosen))
      (map car chosen))
     ;; No more available tiles.
     ((null? available)
      (if (not (null? rejected))
          ;; If we previously rejected some tiles, consider them
          ;; available again (give a "second chance") and continue.
          (recur rejected chosen '())
          ;; No rejected tiles either. We have chosen every
          ;; tile, so there is nothing we can do except return.
          (map car chosen)))
     ;; Test the first available tile. If we choose it, remove
     ;; it from the available list, add it to the chosen list,
     ;; and continue.
     ((%choose? (car available))
      (recur (cdr available)
             (cons (car available) chosen)
             rejected))
     ;; Otherwise we didn't choose the tile. Remove it from the
     ;; available list, add it to the rejected list, and continue.
     (else
      (recur (cdr available)
             chosen
             (cons (car available) rejected))))))


;; If the tile has a treasure spawn point label, return that.
;; Otherwise, pretend it is the first kind of treasure spawn.
(define (treasure-spawn-label-for-tile tile)
  (let ((label (tile-label tile)))
    (if (member label +treasure-spawn-labels+)
        label
        (first +treasure-spawn-labels+))))


;; Return an entry from +treasure-table+ appropriate for the tile.
(define (treasure-chance-for-tile tile)
  (let* ((label  (treasure-spawn-label-for-tile tile))
         (entry  (alist-ref label +treasure-table+ eqv? '()))
         (chance (alist-ref 'chance entry)))
    (or chance
        (error "Could not find treasure chance for tile"
               tile))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CHOOSE TREASURE TYPE
;;;
;;; These procedures are used after a treasure spawn point has been
;;; chosen, to choose what type of treasure is spawned there.

;;; Choose a random treasure type appropriate for the given tile. If
;;; the tile is not a treasure spawn point, we pretend that it is the
;;; first kind of treasure spawn point.
(define (choose-treasure-type-for-tile tile)
  (choose-treasure-type-for-label
   (treasure-spawn-label-for-tile tile)))

;;; Choose a random treasure type appropriate for the given treasure
;;; spawn label. Returns #f if label is not a treasure spawn label.
(define (choose-treasure-type-for-label label)
  (let ((entry (alist-ref label +treasure-table+)))
    (if (not entry)
        #f
        (car (list-pick-random
              (treasures-in-value-range
               (alist-ref 'min-value entry)
               (alist-ref 'max-value entry)))))))


;;; Returns a list of all treasure types that have a value in the
;;; given range (inclusive).
(define (treasures-in-value-range min-value max-value)
  (filter (lambda (pair)
            (and (<= min-value (cdr pair) max-value)))
          +treasure-values+))
