;;; This is an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(import scheme)
(cond-expand
  (chicken-4
   (use (prefix sdl2 sdl2:)
        (prefix sdl2-image img:)
        data-structures
        extras
        ports
        srfi-1
        miscmacros))
  (chicken-5
   (import (chicken condition)
           (chicken fixnum)
           (chicken format)
           (chicken pretty-print)
           (chicken sort)
           (prefix sdl2 sdl2:)
           (prefix sdl2-image img:)
           (srfi 1)
           (srfi 18)
           miscmacros)))

(include "../shared/lib/math.scm")
(include "../shared/lib/util.scm")
(include "../shared/lib/ecs.scm")
(import (prefix ecs ecs:))

;;; World coordinates <--> screen coordinates
(define (w->s n) (/ n 1000))
(define (s->w n) (* n 1000))
(define (w->s/p p) (sdl2:point-unscale p 1000))
(define (s->w/p p) (sdl2:point-scale   p 1000))
(define (w->s/r r) (sdl2:rect-unscale  r 1000))
(define (s->w/r r) (sdl2:rect-scale    r 1000))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION

;; Initialize SDL
(sdl2:set-main-ready!)
(sdl2:init! '(video events timer))

;; Schedule quit! to be automatically called when the program exits.
(on-exit sdl2:quit!)

;; Ensure that quit! will be called even if an unhandled exception
;; reaches the top level.
(current-exception-handler
 (let ((original-handler (current-exception-handler)))
   (lambda (exception)
     (sdl2:quit!)
     (original-handler exception))))

;;; Request to use the best available render quality for scaling and
;;; rotating textures.
(sdl2:set-hint! 'render-scale-quality "best")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW / RENDERER

(define +window-w+ 800)
(define +window-h+ 600)

(define *window*
  (make-parameter
   (sdl2:create-window!
    "Shmup"
    'undefined 'undefined
    +window-w+ +window-h+)))

(define *renderer*
  (make-parameter
   (sdl2:create-renderer!
    (*window*)
    -1
    '(accelerated))))


(include "src/textures.scm")
(include "src/entity.scm")
(include "src/players.scm")
(include "src/lasers.scm")
(include "src/enemies/bouncer.scm")
(include "src/event-handling.scm")
(include "src/sky.scm")
(include "src/systems/collision.scm")
(include "src/systems/render.scm")
(include "src/scene.scm")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAIN LOOP

(define *old-ticks* (make-parameter 0))

;; Return the number of seconds that have passed since the last time
;; this procedure was called.
(define (tick!)
  (let* ((new-ticks (sdl2:get-ticks))
         (dt (/ (- new-ticks (*old-ticks*)) 1000)))
    (*old-ticks* new-ticks)
    dt))


(define *exit-main-loop!* (make-parameter void))

(define (main-loop)
  ;; Ignore any time that passed while not in the main loop.
  (*old-ticks* (sdl2:get-ticks))

  ;; Initialize the global event handlers.
  (*event-handlers* (build-event-handlers))

  (*scene* (make-scene bounds: (s->w/r (R 0 0 +window-w+ +window-h+))))
  (populate-scene! (*scene*))

  ;; Create a continuation that can be called to exit the main loop.
  (let/cc exit-main-loop!
    ;; Put the continuation in a parameter so event handlers can
    ;; easily access it.
    (*exit-main-loop!* exit-main-loop!)

    ;; Loop forever, until (*exit-main-loop!*) is called.
    (while #t
      ;;; Handle every pending event.
      (sdl2:pump-events!)
      (while (sdl2:has-events?)
        (handle-event! (sdl2:poll-event!)
                       (*event-handlers*)))

      ;; Update the game state.
      (scene-update! (*scene*) (tick!))

      ;; Redraw the scene.
      (render-scene! (*scene*) (*renderer*))

      ;; Pause briefly to let the CPU rest.
      (thread-sleep! 0.005))))


;;; Start the main loop in a background thread. This is useful at the
;;; REPL so you can interact with the game while it is running.
(define (start-main-loop-in-thread!)
  (*main-loop-thread* (thread-start! main-loop)))

(define *main-loop-thread* (make-parameter #f))


(cond-expand
  ;; If compiled or run with "-D geiser" flag, start the main loop in
  ;; a background thread. This works best when using Geiser.
  (geiser
   (start-main-loop-in-thread!))
  ;; If compiled or run with "-D repl" flag, start the main loop in a
  ;; background thread, then launch a REPL.
  (repl
   (start-main-loop-in-thread!)
   (repl))
  ;; Otherwise, just start the main loop in the foreground thread.
  (else
   (main-loop)))
