;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A tile represents a small square piece of a level, such as a block
;;; of dirt or a tree or some water. Tiles can also have other
;;; behavior, such as being a spawn point for entities such as players
;;; or items.
;;;
;;; A tile may optionally have a three-character label symbol which is
;;; used to add special properties or behavior to the tile. Labels are
;;; usually lowercase, to visually distinguish them from tile types.
;;; The same label can be used by multiple tiles in the level. If a
;;; tile does not need special behaviors or properties, its label is
;;; usually #f (the false value). In the level format, label string
;;; "~~~" becomes label value #f.
;;;
;;; A tile has a tiletype which describes its appearance and default
;;; properties.
;;;
;;; A tile has a col (column) and row, which specify its position
;;; within the level.
;;;
;;; A tile has an alist of extra properties which can be used by the
;;; game logic. For example it might have a property indicating that
;;; this tile is nonsolid (entities can pass through it), or that this
;;; tile harms players who touch it. A property in an individual tile
;;; takes precedence over a property of the same name in the tiletype.


(define-record-type tile
  (%make-tile label type col row props)
  tile?
  (label   tile-label   (setter tile-label))  ; symbol or #f
  (type    tile-type    (setter tile-type))   ; a tiletype
  (col     tile-col     (setter tile-col))    ; column within level
  (row     tile-row     (setter tile-row))    ; row within level
  (bbox    tile-bbox    (setter tile-bbox))   ; cached tile bbox
  (hitbox  tile-hitbox  (setter tile-hitbox)) ; cached tile hitbox
  (props   tile-props   (setter tile-props))) ; properties alist

(define-type tile (struct tile))


(: make-tile
   ((or symbol boolean) tiletype fixnum fixnum
    #!optional (list-of (pair symbol *))
    -> tile))
(define (make-tile label type col row #!optional (props '()))
  (unless (or (not label)
              (and (symbol? label)
                   (= 3 (string-length (symbol->string label)))))
    (error "tile label must be symbol with 3 chars" label))
  (%make-tile label type col row props))


(define-record-printer (tile t out)
  (fprintf out "#<tile ~S,~S ~A~A>"
           (tile-col t)
           (tile-row t)
           (sprintf "type: ~S" (tiletype-id (tile-type t)))
           (if (tile-label t) (sprintf " label: ~S" (tile-label t)) "")))


;;; Get a named property of the tile. If the property is not found in
;;; the tile, the tiletype will be checked. Returns default if the
;;; property is not found in either the tile or the tiletype.
(: tile-prop
   (tile symbol #!optional * -> *))
(define (tile-prop tile name #!optional default)
  (alist-ref name (tile-props tile) eqv?
             (tiletype-prop (tile-type tile) name default)))

;;; Set a property of the tile.
(: tile-prop-set!
   (tile symbol * -> void))
(define (tile-prop-set! tile name value)
  (set! (tile-props tile)
        (alist-update! name value (tile-props tile))))

(set! (setter tile-prop) tile-prop-set!)


;;; Draw the tile to the destination surface. off-x and off-y are
;;; screenspace offsets for drawing.
(define (draw-tile! tile dst #!optional (off-x 0) (off-y 0))
  (draw-tiletype! (tile-type tile) dst
                  (+ off-x (g->s (tile-col tile)))
                  (+ off-y (g->s (tile-row tile)))))


;;; Return a rect representing the tile's bounding box in worldspace.
(define (calculate-tile-bbox tile)
  (g->w/r (R (tile-col tile) (tile-row tile) 1 1)))


;;; Return a rect representing the tile's hitbox in worldspace.
;;; This is affected by the tile's shape property.
(define (calculate-tile-hitbox tile)
  (case (tile-prop tile 'shape)
    ((top-half)
     (R (g->w (tile-col tile))
        (g->w (tile-row tile))
        (g->w 1)
        (round (g->w 0.5))))
    ((bottom-half)
     (R (g->w (tile-col tile))
        (round (g->w (+ 0.5 (tile-row tile))))
        (g->w 1)
        (round (g->w 0.5))))
    ((bottom-third)
     (R (g->w (tile-col tile))
        (round (g->w (+ 0.67 (tile-row tile))))
        (g->w 1)
        (round (g->w 0.33))))
    (else
     ;; No shape or unrecognized shape. Use the whole bounding box.
     (tile-bbox tile))))
