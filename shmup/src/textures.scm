;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define (load-texture path)
  (sdl2:create-texture-from-surface (*renderer*) (img:load path)))

(define (asset-path relative-path)
  (string-append "../shared/assets/" relative-path))


(define +textures+
  (map
   (lambda (pair)
     (cons (car pair) (load-texture (asset-path (cdr pair)))))
   `((background    . "kenney/space_shooter/backgrounds/blue.png")
     (player-blue   . "kenney/space_shooter/players/playerShip3_blue.png")
     (player-red    . "kenney/space_shooter/players/playerShip3_red.png")
     (enemy-black4  . "kenney/space_shooter/enemies/enemyBlack4.png")
     (laser-blue    . "kenney/space_shooter/lasers/laserBlue01.png")
     (laser-red     . "kenney/space_shooter/lasers/laserRed01.png"))))

(define (get-texture symbol)
  (alist-ref/required symbol +textures+))


(define (player-texture color)
  (case color
    ((blue) (get-texture 'player-blue))
    ((red)  (get-texture 'player-red))
    (else (error "Unknown player color" color))))

(define (laser-texture color)
  (case color
    ((blue) (get-texture 'laser-blue))
    ((red)  (get-texture 'laser-red))
    (else (error "Unknown laser color" color))))
