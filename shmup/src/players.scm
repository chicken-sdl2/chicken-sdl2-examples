;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define +player-speed+ (s->w 200))
(define +player-cooldown-secs+ 0.125)

(define (make-player #!key id color
                     (pos (P 0 0))
                     (vel (P 0 0)))
  (make-entity
   id:      id
   type:    'player
   updater: player-updater
   texture: (player-texture color)
   pos:     pos
   vel:     vel
   scale:   0.5
   props:   (list (cons 'color color))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UPDATER

(define (player-updater player dt)
  (player-apply-cooldown! player dt)
  (when (player-shooting? player)
    (player-try-shoot! player))
  (update-entity-pos! player dt)
  (entity-clamp-to-rect! player (scene-bounds (*scene*))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MOVEMENT

;;; List-set of directions player is holding: '(up down left right)
(define-entity-prop player-dirs 'dirs '())

;;; Calculate velocity based on the directions player is holding.
(define (player-update-vel! player)
  (let ((vel (entity-vel player)))
    (sdl2:point-set! vel 0 0)
    (for-each
     (lambda (dir)
       (case dir
         ((left)  (dec! (sdl2:point-x vel)))
         ((right) (inc! (sdl2:point-x vel)))
         ((up)    (dec! (sdl2:point-y vel)))
         ((down)  (inc! (sdl2:point-y vel)))
         (else (error "Unknown dir" dir))))
     (player-dirs player))
    (sdl2:point-scale! vel +player-speed+)))

(define (player-start-move! player dir)
  (set! (player-dirs player)
    (lset-adjoin eq? (player-dirs player) dir))
  (player-update-vel! player))

(define (player-stop-move! player dir)
  (set! (player-dirs player)
    (delete! dir (player-dirs player) eq?))
  (player-update-vel! player))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SHOOTING

(define-entity-prop player-shooting? 'shooting? #f)
(define-entity-prop player-cooldown 'cooldown 0)

(define (player-start-shoot! player)
  (set! (player-shooting? player) #t)
  (player-try-shoot! player))

(define (player-stop-shoot! player)
  (set! (player-shooting? player) #f))

(define (player-try-shoot! player)
  (unless (positive? (player-cooldown player))
    (spawn-laser! (*scene*) player)
    (inc! (player-cooldown player) +player-cooldown-secs+)))

(define (player-apply-cooldown! player dt)
  (when (positive? (player-cooldown player))
    (dec! (player-cooldown player) dt)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; USER INPUT

;;; Returns a list of event handler procedures for moving a player.
(define (make-player-scancode-handlers
         player-id #!key up down left right shoot)
  (list
   (make-player-move-scancode-handler
    player-id  up    'up)
   (make-player-move-scancode-handler
    player-id  down  'down)
   (make-player-move-scancode-handler
    player-id  left  'left)
   (make-player-move-scancode-handler
    player-id  right 'right)
   (make-player-shoot-scancode-handler
    player-id  shoot)))


(define (make-player-scancode-handler
         player-id scancode
         #!key on-press on-release)
  (make-scancode-handler
   scancode
   on-press: (lambda (ev)
               (with-scene-entity
                (*scene*) player-id
                (lambda (player)
                  (on-press player)
                  +event-consumed+)))
   on-release: (lambda (ev)
                 (with-scene-entity
                  (*scene*) player-id
                  (lambda (player)
                    (on-release player)
                    +event-consumed+)))))


;;; Returns an event handler procedure that causes a player to start
;;; or stop moving when a certain key scancode is pressed or released.
(define (make-player-move-scancode-handler
         player-id scancode dir)
  (make-player-scancode-handler
   player-id scancode
   on-press:   (cut player-start-move! <> dir)
   on-release: (cut player-stop-move!  <> dir)))


(define (make-player-shoot-scancode-handler player-id scancode)
  (make-player-scancode-handler
   player-id scancode
   on-press:   (cut player-start-shoot! <>)
   on-release: (cut player-stop-shoot!  <>)))

