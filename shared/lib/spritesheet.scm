;;; This is an example library for use with chicken-sdl2.
;;; It is distributed with the chicken-sdl2 examples:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A sprite sheet is an image that contains many smaller sub-images,
;;; often arranged in a grid. This module provides a spritesheet
;;; record type and procedures to make it easier to use a sprite
;;; sheet.
;;;
;;; The workflow is this:
;;;
;;;   1. Load your sprite sheet image file as an sdl2:surface, e.g.
;;;      using the sdl2-image library.
;;;
;;;   2. Use make-spritesheet to make a spritesheet with that surface.
;;;
;;;   3. Define each sprite using spritesheet-add-grid-sprites!,
;;;      spritesheet-add-grid-sprite! or spritesheet-add-rect-sprite!.
;;;      Each sprite has a name (usually a symbol) and a rect
;;;      specifying the sprite's location and dimensions in the
;;;      original image. The sprites are stored internally as
;;;      surfslices (from the surfslice module).
;;;
;;;   4. When you need to draw a sprite, fetch it by name using
;;;      spritesheet-ref, then blit it using blit-surfslice! (from the
;;;      surfslice module).
;;;
;;; This module depends on the surfslice module (surfslice.scm).


(module spritesheet
    (grid-rect

     make-spritesheet
     spritesheet?
     spritesheet-surface
     spritesheet-sprites

     spritesheet-ref
     spritesheet-ref/default

     spritesheet-add-grid-sprites!
     spritesheet-add-grid-sprite!
     spritesheet-add-rect-sprite!
     spritesheet-add-sprite!)


  (import scheme)
  (cond-expand
    (chicken-4
     (import chicken)
     (use (prefix sdl2 sdl2:)
          data-structures
          extras))
    (chicken-5
     (import (chicken base)
             (chicken format)
             (prefix sdl2 sdl2:))))

  (import surfslice)


  ;; Returns an sdl2:rect for an individual cell in a grid-based
  ;; spritesheet. This is used by spritesheet-add-grid-sprite! to
  ;; calculate the sprite rect.
  ;;
  ;; - col and row specify the column (x) and row (y) of the sprite,
  ;;   with the top-left-most sprite being column 0, row 0.
  ;;
  ;; - cell-w and cell-h specify the size of each cell in the grid (not
  ;;   including gap between cells)
  ;;
  ;; - gap-x and gap-y specify the number of pixels of gap between
  ;;   neighboring cells on the grid. E.g. 1 means there is 1 pixel of
  ;;   space between neighboring cells.
  ;;
  ;; - offset-x and offset-y specify the top left corner of the
  ;;   top-left-most cell, in pixels.
  ;;
  ;; For example, suppose each cell is 8px wide and 16px high, with
  ;; 1px gap between cells, and the top-left cell is offset 5px left
  ;; and 10px down from the top left corner of the image. In such
  ;; case, cell-w is 8, cell-h is 16, gap-x and gap-y are both 1,
  ;; offset-x is 5, and offset-y is 10.
  ;;
  (define (grid-rect #!key col row cell-w cell-h
                     (gap-x 0) (gap-y 0) (offset-x 0) (offset-y 0))
    (assert col    "col is required")
    (assert row    "row is required")
    (assert cell-w "cell-w is required")
    (assert cell-h "cell-h is required")
    (sdl2:make-rect (+ offset-x (* col (+ gap-x cell-w)))
                    (+ offset-y (* row (+ gap-y cell-h)))
                    cell-w
                    cell-h))


  ;; spritesheet record type.
  ;;
  ;; - surface specifies the surface containing the sprite sheet image.
  ;;
  ;; - sprites holds an a-list of (sprite-name . surfslice) pairs for
  ;;   the sprites in the sheet.
  ;;
  (define-record-type spritesheet
    (%make-spritesheet surface sprites)
    spritesheet?
    (surface spritesheet-surface (setter spritesheet-surface))
    (sprites spritesheet-sprites (setter spritesheet-sprites)))

  (define-record-printer (spritesheet x port)
    (fprintf port "#<spritesheet (~S sprites)>"
             (length (spritesheet-sprites x))))

  (define (make-spritesheet surface)
    (%make-spritesheet surface '()))


  ;; Return the surfslice for the sprite with this name. Signals an
  ;; error if no such sprite exists in this spritesheet.
  (define (spritesheet-ref sheet sprite-name)
    (or (spritesheet-ref/default sheet sprite-name #f)
        (error (sprintf "No sprite named ~S in spritesheet" sprite-name)
               sheet)))

  ;; Return the surfslice for the sprite with this name. Returns the
  ;; default value if no such sprite exists in this spritesheet.
  (define (spritesheet-ref/default sheet sprite-name default)
    (alist-ref sprite-name (spritesheet-sprites sheet) eqv? default))



  ;; Add multiple sprites to the spritesheet, calculating their rects
  ;; based on the grid settings.
  ;;
  ;; - shared-grid-args must be a plist of args to pass to grid-rect,
  ;;   which are used for every sprite.
  ;;
  ;; - sprites must be a list of (sprite-name sprite-grid-args...)
  ;;   lists. sprite-grid-args must be args to pass to grid-rect,
  ;;   which are are used for that sprite only.
  ;;
  ;; sprite-grid-args and shared-grid-args are combined to make the
  ;; complete grid-rect args list for that sprite. If the same key
  ;; appears in both the sprite-grid-args and shared-grid-args, the
  ;; value from sprite-grid-args takes precedence.
  ;;
  ;; Example:
  ;;
  ;;   (spritesheet-add-grid-sprites!
  ;;    my-spritesheet
  ;;    '(cell-w: 32 cell-h: 32 gap-x: 1 gap-y: 1)
  ;;    '((chicken col: 0 row: 0)
  ;;      (horse   col: 1 row: 0)))
  ;;      (cow     col: 0 row: 1)))
  ;;
  (define (spritesheet-add-grid-sprites! sheet shared-grid-args sprites)
    (for-each (lambda (sprite)
                (let ((sprite-name      (car sprite))
                      (sprite-grid-args (cdr sprite)))
                  (spritesheet-add-rect-sprite!
                   sheet sprite-name
                   (apply grid-rect
                     (append sprite-grid-args shared-grid-args)))))
              sprites))


  ;; Add a sprite to the spritesheet, calculating its rect based on
  ;; the grid settings. grid-args must be args to pass to the
  ;; grid-rect procedure (see above).
  (define (spritesheet-add-grid-sprite! sheet sprite-name . grid-args)
    (spritesheet-add-rect-sprite!
     sheet sprite-name
     (apply grid-rect grid-args)))


  ;; Add a sprite to the spritesheet, using the given rect. This gives
  ;; you more flexibility than spritesheet-add-grid-sprite!, but you
  ;; must measure the sprite rect yourself.
  (define (spritesheet-add-rect-sprite! sheet sprite-name rect)
    (spritesheet-add-sprite!
     sheet sprite-name
     (make-surfslice (spritesheet-surface sheet) rect)))


  ;; Add any surfslice as a sprite to the spritesheet. This gives you
  ;; the most flexibility. The slice can even use a different surface
  ;; than the spritesheet, if you want.
  (define (spritesheet-add-sprite! sheet sprite-name slice)
    (set! (spritesheet-sprites sheet)
          (cons (cons sprite-name slice)
                (spritesheet-sprites sheet))))

  ) ;; end module
