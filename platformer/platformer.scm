;;; This is an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(cond-expand
  (chicken-4
   (use (prefix sdl2 sdl2:)
        (prefix sdl2-image img:)
        data-structures
        extras
        ports
        srfi-1
        srfi-18
        args
        miscmacros
        vector-lib))
  (chicken-5
   (import (chicken condition)
           (chicken fixnum)
           (chicken format)
           (chicken io)
           (chicken memory)
           (chicken process-context)
           (rename (chicken random)
                   (pseudo-random-integer random))
           (chicken sort)
           (prefix sdl2 sdl2:)
           (prefix sdl2-image img:)
           (srfi 1)
           (srfi 18)
           args
           miscmacros
           vector-lib)
   (define (read-file path)
     (call-with-input-file path read-list))))


(include "../shared/lib/util.scm")
(include "../shared/lib/math.scm")
(include "../shared/lib/surfslice.scm")
(include "../shared/lib/spritesheet.scm")
(import surfslice spritesheet)

(include "src/coordinates.scm")
(include "src/body.scm")
(include "src/cshape.scm")
(include "src/debug-overlay.scm")
(include "src/tiletype.scm")
(include "src/tile.scm")
(include "src/level.scm")
(include "src/load-level.scm")
(include "src/entity.scm")
(include "src/player.scm")
(include "src/player-event.scm")
(include "src/treasure.scm")
(include "src/tile-collision.scm")
(include "src/entity-collision.scm")
(include "src/scene.scm")
(include "src/user-input.scm")
(include "src/event-handling.scm")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COMMAND LINE ARGS

(define *level-file* (make-parameter "levels/level1.txt"))

(define opts
  (list
   (args:make-option
    (l level)  (required: "FILE")
    (sprintf "Level file to load [Default: ~A]" (*level-file*)))
   (args:make-option
    (h help)  #:none
    "Display this help text"
    (print-usage))))

(define (print-usage)
 (with-output-to-port (current-error-port)
   (lambda ()
     (print "Usage: " (program-name) " [OPTIONS...]")
     (newline)
     (print (args:usage opts))))
 (exit 1))


(receive (options operands)
    (args:parse (command-line-arguments) opts)
  (*level-file* (or (alist-ref 'level options) (*level-file*))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TILETYPES

(define *tiletypes*
  (make-parameter
   (with-error-context
    "While loading \"data/tiletypes.scm\" ..."
    (map (lambda (typeargs)
           (cons (car typeargs) (apply make-tiletype typeargs)))
         (read-file "data/tiletypes.scm")))))

(define (get-tiletype id)
  (or (alist-ref id (*tiletypes*))
      (error (sprintf "No tiletype ~S in types" id)
             (*tiletypes*))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOAD LEVEL

(define *level*
  (make-parameter
   (with-error-context
       (sprintf "While loading ~S ..." (*level-file*))
     (load-level (*level-file*)))))

;; (print (*level*))
;; (pp (level-props (*level*)))
;; (pp (level-tiles (*level*)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SETTINGS

(define *settings*
  (with-error-context
   "While loading \"settings.txt\" ..."
   (read-file "settings.txt")))

(define (get-setting name #!optional default)
  (let ((setting (alist-ref name *settings*)))
    (if setting (car setting) default)))

(define (set-setting! name value)
  (set! *settings* (alist-update name (list value) *settings*))
  (void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION

;; Initialize SDL
(sdl2:set-main-ready!)
(sdl2:init! '(video events timer))

;; Schedule quit! to be automatically called when the program exits.
(on-exit sdl2:quit!)

;; Ensure that quit! will be called even if an unhandled exception
;; reaches the top level.
(current-exception-handler
 (let ((original-handler (current-exception-handler)))
   (lambda (exception)
     (sdl2:quit!)
     (original-handler exception))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW

(define (*level-w*) (g->s (level-cols (*level*))))
(define (*level-h*) (g->s (level-rows (*level*))))

(define *window*
  (make-parameter
   (sdl2:create-window!
    "Treasure Jumpers" 'undefined 'undefined
    (floor (* (*level-w*) (get-setting 'window-scale 2)))
    (floor (* (*level-h*) (get-setting 'window-scale 2))))))

;;; The scene is first drawn onto the window buffer, then the window
;;; buffer is blit-scaled onto the window.
(define *window-buffer*
  (make-parameter
   ;; Convert to the same format as the window for faster blitting.
   (sdl2:convert-surface
    (sdl2:make-surface (*level-w*) (*level-h*) 32)
    (sdl2:surface-format (sdl2:window-surface (*window*))))))

;;; Buffer used for drawing the level. Since the level does not
;;; change, it can be rendered once, for efficiency.
(define *level-buffer*
  (make-parameter
   ;; Convert to the same format as the window for faster blitting.
   (sdl2:convert-surface
    (sdl2:make-surface (*level-w*) (*level-h*) 32)
    (sdl2:surface-format (sdl2:window-surface (*window*))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPRITES

;;; Build and return a spritesheet from a list of expressions read
;;; from data/sprites.scm.
(define (build-spritesheet exprs window-format)
  (receive (image-path color-key settings sprites)
      (alist-refs/required
       '(image-path color-key settings sprites) exprs)
    (let ((temp-surf (img:load (car image-path))))
      (set! (sdl2:surface-color-key temp-surf)
            (apply sdl2:make-color color-key))
      (let* ((surf (sdl2:convert-surface temp-surf window-format))
             (sheet (make-spritesheet surf)))
        (spritesheet-add-grid-sprites! sheet settings sprites)
        sheet))))

(define *spritesheet*
  (make-parameter
   (with-error-context
    "While loading \"data/sprites.scm\" ..."
    (build-spritesheet
     (read-file "data/sprites.scm")
     (sdl2:surface-format (sdl2:window-surface (*window*)))))))

(define (blit-sprite! sprite-name dst-surf dst-rect)
  (blit-surfslice!
   (spritesheet-ref (*spritesheet*) sprite-name)
   dst-surf
   dst-rect))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCENE

(draw-level! (*level*) (*level-buffer*))


(define *scene*
  (make-parameter (make-scene (*level*) '())))

(scene-spawn-players!
 (*scene*) (level-prop (*level*) 'players 2))

(scene-spawn-treasure!
 (*scene*)
 (round (* 0.3 (length (level-treasure-spawns (*level*))))))

;; (print (*scene*))
;; (pp (scene-entities (*scene*)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCREENSHOT

;;; Save a BMP screenshot with the contents of the given surface. The
;;; screenshot will be saved in the current directory. Its filename
;;; will be the given prefix plus an integer timestamp of the number
;;; of seconds since 1970-01-01T00:00Z. (This is a weird timestamp
;;; format, but it is not worth requiring the srfi-19 egg and its many
;;; dependencies merely to calculate a timestamp.)
(define (save-screenshot! surf #!optional (prefix "screenshot-"))
  (let* ((timestamp (inexact->exact (current-seconds)))
         (path (sprintf "~A~S.bmp" prefix timestamp)))
    (sdl2:save-bmp! surf path)
    (printf "Saved screenshot: ~A~%" path)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAIN LOOP

(define *old-ticks* (make-parameter 0))

;; Return the number of seconds that have passed since the last time
;; this procedure was called.
(define (tick!)
  (let* ((new-ticks (sdl2:get-ticks))
         (dt (/ (- new-ticks (*old-ticks*)) 1000)))
    (*old-ticks* new-ticks)
    (* dt (get-setting 'time-scale 1))))


(define *exit-main-loop!* (make-parameter void))


(define (refresh! scene level-buffer window-buffer window)
  (sdl2:blit-surface! level-buffer #f window-buffer #f)
  (draw-scene! scene window-buffer)
  (when (need-draw-debug-overlay?)
    (draw-debug-overlay! scene window-buffer))
  (sdl2:blit-scaled! window-buffer #f (sdl2:window-surface window) #f)
  (sdl2:update-window-surface! window))


(define (main-loop)
  ;; Ignore any time that passed while not in the main loop.
  (*old-ticks* (sdl2:get-ticks))

  ;; Initialize the global event handlers.
  (*event-handlers* (build-event-handlers))

  ;; Create a continuation that can be called to exit the main loop.
  (let/cc exit-main-loop!
    ;; Put the continuation in a parameter so event handlers can
    ;; easily access it.
    (*exit-main-loop!* exit-main-loop!)

    ;; Loop forever, until (*exit-main-loop!*) is called.
    (while #t
      ;;; Handle every pending event.
      (sdl2:pump-events!)
      (while (sdl2:has-events?)
        (handle-event! (sdl2:poll-event!) (*event-handlers*)))

      ;; Update the physics engine and game state. The physics is
      ;; updated in small, fixed steps so that collisions are more
      ;; precise and stable.
      (do ((step      0.008               (min step remaining))
           (remaining (min 0.064 (tick!)) (- remaining step)))
          ((> 0.001 remaining))
        (update-scene! (*scene*) step))

      ;; Redraw the scene.
      (refresh! (*scene*) (*level-buffer*) (*window-buffer*) (*window*))

      ;; Pause briefly to let the CPU rest.
      (thread-sleep! 0.010))))


;;; Start the main loop in a background thread. This is useful at the
;;; REPL so you can interact with the game while it is running.
(define (start-main-loop-in-thread!)
  (*main-loop-thread* (thread-start! main-loop)))

(define *main-loop-thread* (make-parameter #f))


;;; If the program is compiled or run with -Drepl, start the main loop
;;; in a background thread, then launch a REPL so the user can type
;;; commands to modify the game in real time. Otherwise, the main loop
;;; will be run in the foreground thread.
(cond-expand
 (repl
  (start-main-loop-in-thread!)
  (repl))
 (else
  (main-loop)))
