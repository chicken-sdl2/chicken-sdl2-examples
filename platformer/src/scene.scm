;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; A scene manages a level and a list of entities. It is responsible
;;; for drawing and updating the state of its level and entities.


(define-record-type scene
  (make-scene level entities)
  scene?
  (level    scene-level)
  (entities scene-entities (setter scene-entities)))

(define-type scene (struct scene))

(: make-scene
   (level list -> scene))

(define-record-printer (scene s out)
  (fprintf out "#<scene ~S (~S entities)>"
           (scene-level s) (length (scene-entities s))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCENE DRAWING

;;; Draw the scene's level and all entities to the dst surface. off-x
;;; and off-y are screenspace offsets for drawing.
(define (draw-scene! scene dst #!optional (off-x 0) (off-y 0))
  (for-each (cut draw-entity! <> dst off-x off-y)
            (scene-entities scene)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ENTITY MANAGEMENT

;;; Find and return the entity with the given id, if such an entity
;;; exists in the scene. Returns #f if no such entity is not found.
(define (scene-find-entity scene id)
  (find (lambda (entity) (eq? id (entity-id entity)))
        (scene-entities scene)))


(define (scene-add-entity! scene entity)
  (set! (scene-entities scene)
    (cons entity (scene-entities scene))))

(define (scene-remove-entity! scene entity)
  (set! (scene-entities scene)
    (delete entity (scene-entities scene) eq?)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SCENE STATE

;;; Update all the entities in the scene. dt is delta time, the number
;;; of seconds that have passed since the last update.
(define (update-scene! scene dt)
  (for-each
   (lambda (entity)
     (if (player? entity)
         (update-player! entity dt)
         (update-entity! entity dt)))
   (scene-entities scene))
  (handle-scene-e/t-collisions! scene dt)
  (handle-scene-e/e-collisions! scene dt)
  (check-for-fallen-players scene))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ENTITY/TILE COLLISIONS

(define (handle-scene-e/t-collisions! scene dt)
  (for-each
   (lambda (entity)
     (when (player? entity)
       (for-each
        (lambda (tile)
          (let ((colls (check-e/t-colls entity tile)))
            (player-resolve-e/t-colls! entity tile colls dt)))
        (nearby-solid-tiles entity (scene-level scene)))))
   (scene-entities scene)))


;;; Returns the first entity in the scene whose bbox intersects with
;;; the given tile's bbox, or #f if no entity intersects.
(define (scene-entity-at-tile scene tile)
  (let ((tbb (tile-bbox tile)))
    (find (lambda (entity)
            (sdl2:has-intersection? (entity-bbox entity) tbb))
          (scene-entities scene))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ENTITY/ENTITY COLLISIONS

(define (handle-scene-e/e-collisions! scene dt)
  (for-each
   (lambda (pair)
     (let ((e1 (car pair))
           (e2 (cdr pair)))
       (cond
        ;; ((and (player? e1) (player? e2))
        ;;  (handle-scene-p/p-collision! scene dt e1 e2))
        ((and (treasure? e1) (player? e2))
         (handle-scene-t/p-collision! scene dt e1 e2))
        ;; ((and (treasure? e1) (treasure? e2))
        ;;  (handle-scene-t/t-collision! scene dt e1 e2))
        )))
   (map %treasure-first
        (broad-e/e-colls (scene-entities scene)))))

;;; Make sure that if the collision involves a player and a treasure,
;;; the treasure is the car and the player is the cdr.
(define (%treasure-first pair)
  (if (and (player? (car pair))
           (treasure? (cdr pair)))
      (cons (cdr pair) (car pair))
      pair))


;;; TODO: Handle collision between two players.
;; (define (handle-scene-p/p-collision! scene dt p1 p2)
;;   ...)


;;; Handle collision between treasure and player.
(define (handle-scene-t/p-collision! scene dt t p)
  (player-got-treasure! p (treasure-value t))
  (scene-remove-entity! scene t)
  ;; Spawn a replacement treasure.
  (scene-spawn-treasure! scene 1))


;;; TODO: Handle collision between two treasures.
;; (define (handle-scene-t/t-collision! scene dt p1 p2)
;;   ...)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPAWNING PLAYERS

;;; Create and initialize the given number of players, add them to the
;;; scene, and return them.
(define (scene-spawn-players! scene num-players)
  (let ((grav (level-gravity (scene-level scene))))
    (map (lambda (id)
           (let* ((color (alist-ref id +player-default-colors+))
                  (player (make-player id color))
                  (spawns (scene-player-spawns scene player)))
             (entity-move-to-tile! player (list-pick-random spawns))
             (entity-force-set! player 'gravity grav)
             (scene-add-entity! scene player)
             player))
         (take +player-possible-ids+ num-players))))


;;; Check whether any of the players have fallen out of bounds.
(define (check-for-fallen-players scene)
  (let ((max-y (+ (level-height (scene-level scene))
                  (s->w 300))))
    (for-each
     (lambda (entity)
       (when (and (player? entity)
                  (> (entity-py entity) max-y))
         (player-out-of-bounds! entity)))
     (scene-entities scene))))


;;; Find a respawn point, then respawn the player there.
(define (scene-respawn-player! scene player)
  (player-respawn!
   player (list-pick-random
           (scene-player-respawns scene player))))


;;; Returns a list of valid initial spawn points for the player. First
;;; looks for matching primary spawn points. If none are found, looks
;;; for secondary spawn points. If none are found, prints a warning
;;; and returns a list of one tile in the middle of the level.
(define (scene-player-spawns scene player)
  (let* ((level (scene-level scene))
         (primaries (level-player-primary-spawns level player))
         (secondaries (level-player-secondary-spawns level)))
    (cond
     ((not (null? primaries))
      primaries)
     ((not (null? secondaries))
      secondaries)
     (else
      (fprintf (current-error-port)
        "Warning: no spawn point found for player: ~S" player)
      (list (level-middle-tile level))))))


;;; Returns a list of all valid respawn points for the player. That
;;; means any matching primary spawn points, and any secondary spawn
;;; points. If no respawn points are found, prints a warning and
;;; returns a list of one tile in the middle of the level.
(define (scene-player-respawns scene player)
  (let* ((level (scene-level scene))
         (tiles (append (level-player-primary-spawns level player)
                        (level-player-secondary-spawns level))))
    (if (not (null? tiles))
        tiles
        (begin
          (fprintf (current-error-port)
            "Warning: no respawn point found for player: ~S" player)
          (list (level-middle-tile level))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPAWNING TREASURE

;;; Spawn treasure entities at random treasure spawn points. They are
;;; added as entities to the scene and returned.
(define (scene-spawn-treasure! scene num-treasures)
  (let ((tiles (scene-choose-treasure-spawns scene num-treasures)))
    (map (lambda (tile)
           (let ((entity (make-treasure
                          (choose-treasure-type-for-tile tile))))
             (entity-move-to-tile! entity tile)
             (scene-add-entity! scene entity)
             entity))
         tiles)))


;;; Randomly choose N tiles to spawn treasures at. Will not choose a
;;; treasure point that has an entity (treasure or player) there. If
;;; no suitable tiles are found, it will choose N random tiles.
(define (scene-choose-treasure-spawns scene n)
  (let* ((level (scene-level scene))
         (tiles (filter (lambda (tile)
                          (not (scene-entity-at-tile scene tile)))
                        (level-treasure-spawns level))))
    (if (null? tiles)
        (list-tabulate n (lambda (_) (level-random-tile level)))
        (choose-treasure-spawns n tiles))))
