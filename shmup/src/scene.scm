;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define *scene* (make-parameter (void)))


(define-record-type scene
  (%make-scene entities db bounds sky)
  scene?
  ;; List of entity instances (TODO: eliminate need for this)
  (entities  scene-entities  (setter scene-entities))
  ;; ECS database
  (db        scene-db        (setter scene-db))
  ;; sdl2:rect defining level boundaries
  (bounds    scene-bounds    (setter scene-bounds))
  (sky       scene-sky       (setter scene-sky))
  )

(define (make-scene #!key bounds)
  (%make-scene
   '()
   (ecs:make-db)
   bounds
   (make-sky (get-texture 'background))))


(define (scene-add-entity! scene entity comps)
  (ecs:add-components! (scene-db scene) entity comps)
  (printf "Added entity ~S with components:~N" entity)
  (pretty-print (ecs:entity-all-components (scene-db scene) entity))
  (set! (scene-entities scene)
    (cons entity (scene-entities scene))))

(define (scene-remove-entity! scene entity)
  (ecs:forget-entity! (scene-db scene) entity)
  (printf "Removed entity ~S~N" entity)
  (set! (scene-entities scene)
    (delete! entity (scene-entities scene))))

(define (scene-find-entity scene id)
  (find (lambda (entity) (eq? (entity-id entity) id))
        (scene-entities scene)))

(define (with-scene-entity scene id proc)
  (let ((entity (scene-find-entity (*scene*) id)))
    (and entity (proc entity))))


;;; dt is delta time, the number of seconds that have passed since
;;; last update.
(define (scene-update! scene dt)
  (update-sky! (scene-sky scene) dt)
  (for-each (cut update-entity! <> dt)
            (scene-entities scene))
  (scene-check-collisions! scene))

(define (scene-check-collisions! scene)
  (for-each
   (lambda (coll)
     ;; Inform each entity that it has collided with the other.
     (let ((a (car coll))
           (b (cdr coll)))
       (entity-collision! a b)
       (entity-collision! b a)))
   (broad-e/e-colls scene)))


;;; Populate the scene with some entities.
(define (populate-scene! scene)
  (for-each
   (cut apply scene-add-entity! scene <>)
   (list
    ;; Player 1
    (make-player
     id:     'player1
     color:  'blue
     pos:    (s->w/p (P 300 400)))

    ;; Player 2
    (make-player
     id:     'player2
     color:  'red
     pos:    (s->w/p (P 500 400)))

    (make-bouncer
     pos: (s->w/p (P 200 200))
     vel: (s->w/p (P 120 50)))
    ))
  scene)
