;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


(define +sky-scroll-speed+ 30)


(define-record-type sky
  (%make-sky texture offset)
  sky?
  (texture  sky-texture  (setter sky-texture))
  (offset   sky-offset   (setter sky-offset))
  )


(define (make-sky texture)
  (%make-sky texture 0))


(define (update-sky! sky dt)
  (inc! (sky-offset sky) (* dt +sky-scroll-speed+))
  (let ((th (sdl2:texture-h (sky-texture sky))))
    (when (>= (sky-offset sky) th)
      (dec! (sky-offset sky) th))))


;;; Draw sky on ren (a renderer), filling the current viewport with
;;; the tiled texture.
(define (draw-sky! sky ren)
  (let ((texture  (sky-texture sky))
        (viewport (sdl2:render-viewport ren)))
    (let ((tw (sdl2:texture-w texture))
          (th (sdl2:texture-h texture))
          (vw (sdl2:rect-w viewport))
          (vh (sdl2:rect-h viewport)))
      (let ((rect  (sdl2:make-rect))
            (off-y (round (sky-offset sky))))
        (do ((x 0 (+ x tw)))
            ((> x vw))
          (do ((y (- th) (+ y th)))
              ((> y vh))
            (sdl2:rect-set! rect x (+ y off-y) tw th)
            (sdl2:render-copy! ren texture #f rect)))))))
