;;; This is an example library for use with chicken-sdl2.
;;; It is distributed with the chicken-sdl2 examples:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; The ecs module provides lightweight support for "Entity Component
;;; System" (ECS) architectures.
;;;
;;; Original author: John Croisant.
;;;
;;; ECS is a way of organizing your game data and logic. The goal of
;;; ECS is to have very loose coupling between different aspects of
;;; each game object, for flexibility and ease of change.
;;;
;;; There are three core concepts in ECS:
;;;
;;; - Entity: an object in the game (e.g. a player, enemy, or item).
;;;   An entity object does not hold any data or logic. It is merely a
;;;   unique identifier for looking up components in a "database".
;;;
;;; - Component: a collection of data related to a certain aspect of
;;;   an entity (e.g. position, appearance, health). Components only
;;;   hold data, not logic. There can be many types of components.
;;;   Each entity may "own" zero or one instances of each type of
;;;   component.
;;;
;;; - System: logic related to a certain aspect of the game (e.g.
;;;   rendering, collision detection). A system can read and modify
;;;   entities' components to implement gameplay logic.
;;;
;;; This implementation of ECS is very lightweight and flexible:
;;;
;;; - An entity may be any type of unique object, such as a gensym or
;;;   a record type instance. Entities are compared using eq?.
;;;
;;; - A component may be any type of object. It's just data.
;;;
;;; - A system is just a procedure that accesses the ECS database in
;;;   some way.
;;;
;;; This module provides the "db" record type, which represents the
;;; ECS database. It records which components belong to which
;;; entities. Usually there is one db per game (or scene).
;;;
;;; This module also provides many procedures for interacting with a
;;; db, e.g. adding, removing, and querying components. These
;;; procedures can be used in your systems.
;;;
;;; Example:
;;;
;;;   (import (prefix ecs ecs:))
;;;
;;;   ;; Create and set up the ECS db
;;;   (define db (ecs:make-db))
;;;
;;;   ;; Add some components for a player entity.
;;;   ;; In this case, the entity is just a symbol.
;;;   (define player (gensym 'player))
;;;   (ecs:add-components!
;;;    db player
;;;    `((position . #(10 20))
;;;      (texture  . "player1.png")))
;;;
;;;   ;; Add some components for a missile entity.
;;;   (define missile (gensym 'missile))
;;;   (ecs:add-components!
;;;    db missile
;;;    `((position . #(10 50))
;;;      (texture  . "missile.png")
;;;      (owner    . ,player)))
;;;
;;;   ;; The "render" system. It is just a procedure.
;;;   (define (render-system db)
;;;     ;; Iterate over every entity that has both
;;;     ;; texture and position components.
;;;     (ecs:for-each-matching
;;;      db '(texture position)
;;;      ;; This lambda is called with each entity
;;;      ;; and the requested components.
;;;      (lambda (entity txtr posn)
;;;        ;; render! is implemented elsewhere...
;;;        (render! txtr posn))))


(module ecs (make-db
             db?

             add-component!
             add-components!
             remove-component!
             forget-entity!
             forget-all-entities!

             entity-has-component?
             entity-component
             entity-all-components

             for-each-matching
             map-matching
             )

(import scheme)
(cond-expand
  (chicken-4
   (import chicken)
   (use extras srfi-1 srfi-69))
  (chicken-5
   (import (chicken base)
           (chicken type)
           (chicken format)
           (srfi 1)
           (srfi 69))))


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ECSDB

(define-record-type ecs:db
  (%make-db tables)
  db?
  ;; table of component-type => (table of entity => component)
  (tables  %tables))

(define-type ecs:db (struct ecs:db))

(: make-db
   (-> ecs:db))
(define (make-db)
  (%make-db (make-hash-table test: eq? hash: eq?-hash)))

(define-record-printer (ecs:db db out)
  (fprintf out "#<ecs:db (~S types)"
           (hash-table-size (%tables db))))


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ADDING/REMOVING COMPONENTS

;; Add (or replace) a component owned by an entity.
(: add-component!
   (ecs:db * symbol * -> undefined))
(define (add-component! db entity type component)
  (let ((table (%find-or-create-table! db type)))
    (hash-table-set! table entity component)))

;; Add (or replace) multiple components owned by an entity.
(: add-components!
   (ecs:db * (list-of (pair symbol *)) -> undefined))
(define (add-components! db entity comps-alist)
  (for-each
   (lambda (comp-pair)
     (add-component! db entity (car comp-pair) (cdr comp-pair)))
   comps-alist))

;; Remove a component owned by an entity.
(: remove-component!
   (ecs:db * symbol -> undefined))
(define (remove-component! db entity type)
  (let ((table (%table db type)))
    (when table
      (hash-table-delete! table entity))))

;; Remove all components owned by an entity.
(: forget-entity!
   (ecs:db * -> undefined))
(define (forget-entity! db entity)
  (hash-table-for-each (%tables db)
    (lambda (type table)
      (hash-table-delete! table entity))))

;; Remove all components owned by all entities.
(: forget-all-entities!
   (ecs:db -> undefined))
(define (forget-all-entities! db)
  (hash-table-for-each (%tables db)
    (lambda (type table)
      (hash-table-clear! table))))


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; QUERYING SINGLE ENTITIES

;; Return #t if the entity owns a certain type of component.
(: entity-has-component?
   (ecs:db * symbol --> boolean))
(define (entity-has-component? db entity type)
  (and-let* ((table (%table db type)))
    (hash-table-exists? table entity)))

;; Return a certain type of component owned by entity.
;; Returns #f if entity does not own that type of component.
(: entity-component
   (ecs:db * symbol --> any))
(define (entity-component db entity type)
  (and-let* ((table (%table db type)))
    (hash-table-ref/default table entity #f)))

;; Returns list of (type . component) for all components owned by
;; entity.
(: entity-all-components
   (ecs:db * --> (list-of (pair symbol *))))
(define (entity-all-components db entity)
  (remove!
   not
   (hash-table-map (%tables db)
     (lambda (type db)
       (and (hash-table-exists? db entity)
            (cons type (hash-table-ref/default db entity #f)))))))


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; QUERYING MULTIPLE ENTITIES

;; Iterate over every entity that owns all specified component
;; types. Entities that do not own one or more of the specified
;; component types are ignored. The order of entities is
;; unspecified.
;;
;; The proc is called with 1+N arguments: the entity, plus each
;; requested component, in the order requested. E.g. if you request
;; '(foo bar), the procedure will be called with 3 args: the entity,
;; the foo component, and the bar component.
;;
(: for-each-matching
   (ecs:db (list-of symbol) procedure --> undefined))
(define (for-each-matching db types proc)
  (and-let* ((table (%smallest-table db types))
             (candidates (hash-table-keys table)))
    (for-each
     (lambda (entity)
       (and-let* ((comps (%fetch db entity types)))
         (apply proc entity comps)))
     candidates)))


;; Similar to for-each-matching, but returns a list of the results
;; of the procedure calls.
(: map-matching
   (ecs:db (list-of symbol) procedure --> list))
(define (map-matching db types proc)
  (or (and-let* ((table (%smallest-table db types))
                 (candidates (hash-table-keys table)))
        (foldr (lambda (entity accum)
                 (let ((comps (%fetch db entity types)))
                   (if comps
                       (cons (apply proc entity comps)
                             accum)
                       accum)))
               '()
               candidates))
      '()))


;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INTERNAL PROCEDURES (NOT EXPORTED)

;; Return table for component type, or #f if table doesn't exist.
(: %table
   (ecs:db symbol --> (or hash-table false)))
(define (%table db type)
  (hash-table-ref/default (%tables db) type #f))

;; Find or create a table for the component type.
(: %find-or-create-table!
   (ecs:db symbol -> hash-table))
(define (%find-or-create-table! db type)
  (or (%table db type)
      (let ((new-table (make-hash-table test: eq? hash: eq?-hash)))
        (hash-table-set! (%tables db) type new-table)
        new-table)))

;; Find out which of the requested component types has the fewest
;; entities, and return the table for that type. This is used to
;; quickly narrow down the number of entities to check.
(: %smallest-table
   (ecs:db (list-of symbol) --> (or hash-table false)))
(define (%smallest-table db types)
  (let recur ((small-table #f)
              (small-size #f)
              (types types))
    (if (null? types)
        small-table
        (and-let* ((table (%table db (car types)))
                   (size (hash-table-size table)))
          (if (or (not small-size) (< size small-size))
              (recur table       size       (cdr types))
              (recur small-table small-size (cdr types)))))))

;; Find the requested components for the entity. Returns #f if the
;; entity is missing any requested component.
(: %fetch
   (ecs:db * (list-of symbol) --> (or list false)))
(define (%fetch db entity types)
  (let recur ((types types)
              (accum '()))
    (if (null? types)
        (reverse! accum)
        (if (entity-has-component? db entity (car types))
            (recur (cdr types)
                   (cons (entity-component db entity (car types))
                         accum))
            #f))))

) ;; end module
