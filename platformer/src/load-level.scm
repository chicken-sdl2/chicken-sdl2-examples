;;; This is part of an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This file defines the load-level procedure for loading a level
;;; from a file.
;;;
;;; The level format is composed of many s-expressions. Recognized
;;; expressions are:
;;;
;;;   (prop NAME VALUE)               ; Defines a level property
;;;   (row (TYPE ...) [(LABEL ...)])  ; Defines a row of tiles
;;;
;;; Each NAME in a `prop' can be a symbol of the property name, and
;;; each VALUE can be a symbol, string, or number, as appropriate for
;;; that property.
;;;
;;; Each TYPE in a `row' expression must be a tiletype LABEL symbol
;;; (see tiletypes.scm).
;;;
;;; A `row' expression can optionally have a list of LABELs, which are
;;; assigned to the corresponding tile in the row. Each LABEL can
;;; either be the symbol `~~~' which means the tile has no LABEL, or a
;;; three-character symbol.
;;;
;;; The height of the level is determined by the number of `row'
;;; expressions. The width of the level is determined by the number of
;;; tiles in the first row. All rows must have the same number of
;;; tiles.
;;;
;;; Scheme-style comments are allowed in a level.
;;;
;;; Example level:
;;;
;;;   ;; This is a comment.
;;;   (prop title "My First Level")
;;;   (prop author "John Croisant")
;;;   (prop gravity 1.0)
;;;   (prop bg-color (128 0 128))
;;;   (row (--- --- --- --- ---)
;;;        (~~~ T01 ~~~ T02 ~~~))
;;;   (row (--- --- --- --- ---))
;;;   (row (--- --- --- --- ---)
;;;        (~~~ P1~ ~~~ P2~ ~~~))
;;;   (row (GM- GR- WT- GL- GM-))


;;; TODO: Refactor this behemoth.
(: load-level
   (string -> level))
(define (load-level filename)
  ;; Figure out how many columns are in the level.
  (define (%cols row-exprs)
    (length (second (first row-exprs))))

  ;; Figure out how many rows are in the level.
  (define (%rows row-exprs)
    (length row-exprs))

  ;; Convert the prop expressions into an alist.
  (define (%props prop-exprs)
    (map (lambda (prop-expr)
           (cons (second prop-expr) (third prop-expr)))
         prop-exprs))

  ;; The symbol that means the tile has no LABEL.
  (define +no-tile-label+ '~~~)

  ;; Return the LABELs list of the row if it has one. Otherwise return a
  ;; list of '~~~ with the same length as the number of tiles.
  (define (%tile-labels row-expr)
    (if (< 2 (length row-expr))
        (third row-expr)
        (make-list (length (second row-expr)) +no-tile-label+)))

  (define (%validate-level level)
    (when (null? (level-tiles-with-matching-label
                  level +player-spawn-labels+))
      (fprintf (current-error-port)
        "Warning: Level has no player spawn points. Try adding any of these labels to tiles: ~S"
        +player-spawn-labels+))

    (when (null? (level-treasure-spawns level))
      (fprintf (current-error-port)
        "Warning: Level has no treasure spawn points. Try adding any of these labels to tiles: ~S"
        +treasure-spawn-labels+)))

  ;; Read the file and create a level with the correct level id,
  ;; dimensions, and properties, but all empty tiles.
  (let* ((exprs (read-file filename))
         (row-exprs (filter (lambda (expr) (eq? 'row (car expr)))
                            exprs))
         (prop-exprs (filter (lambda (expr) (eq? 'prop (car expr)))
                             exprs))
         (level (make-level filename
                            (%cols row-exprs)
                            (%rows row-exprs)
                            (%props prop-exprs))))

    ;; Iterate through every row and column, updating the tiles.
    (for-each
     (lambda (row-expr row-num)
       (for-each
        (lambda (type-id-sym tile-label-sym col-num)
          (let ((tile (level-ref level col-num row-num)))
            ;; Set the tile's type.
            (set! (tile-type tile) (get-tiletype type-id-sym))
            ;; Set the tile's LABEL if specified.
            (unless (eq? tile-label-sym +no-tile-label+)
              (set! (tile-label tile) tile-label-sym))
            ;; Calculate and cache the tile's bbox and hitbox.
            (set! (tile-bbox tile) (calculate-tile-bbox tile))
            (set! (tile-hitbox tile) (calculate-tile-hitbox tile))))
        (second row-expr)
        (%tile-labels row-expr)
        (iota (length (second row-expr)))))
     row-exprs
     (iota (length row-exprs)))

    (%validate-level level)

    ;; Return the fully loaded level.
    level))
