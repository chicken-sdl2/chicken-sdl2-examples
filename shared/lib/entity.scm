;;; This is an example library for use with chicken-sdl2.
;;; It is distributed with the chicken-sdl2 examples:
;;; https://gitlab.com/chicken-sdl2/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; The entity module provides a basic entity record type, intended
;;; for use with the ecs module. The ecs module allows any type, so
;;; you don't need to use this type, but you can if you want.

(module entity (make-entity
                entity-label)

(import chicken scheme)
(use extras)

(define-record-type entity
  (%make label)
  entity?
  ;; symbol label for debugging purposes
  (label  entity-label  (setter entity-label)))

(define-type entity (struct entity))

(: make-entity
   (#!optional symbol -> entity))
(define (make-entity #!optional label)
  (%make (or label (gensym 'entity))))

(define-record-printer (entity e out)
  (fprintf out "#<entity ~S>" (entity-label e)))

) ;; end module
